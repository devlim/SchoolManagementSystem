<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('parent')->name('parent.')->group(function () {
    Route::get('/inactive', 'Parent\DashboardController@inactive')->name('inactive');

    Route::get('/', 'Parent\DashboardController@index')->name('dashboard');

    Route::prefix('children')->group(function () {
        Route::get('/info', 'Parent\ChildrenController@profile')->name('children.profile');
        Route::get('/activities', 'Parent\ChildrenController@activities')->name('children.activities');
        Route::get('/attendances', 'Parent\ChildrenController@attendances')->name('children.attendances'); //Listing
    });

    Route::prefix('pick_ups')->group(function () {
        Route::get('history', 'Parent\PickUpsController@history')->name('pick_ups.history');
        Route::post('{id}/cancel/', 'Parent\PickUpsController@cancel')->name('pick_ups.cancel');
    });

    Route::resource('pick_ups', 'Parent\PickUpsController', ['only' => [
        'create', 'store', 'show'
    ]]);
});

Route::prefix('admin')->group(function () {
    /* Login/Logout */
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    /* Reset Password */
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

    /* Manage Student*/
    Route::resource('students', 'Admin\StudentsController');
    Route::resource('students.activities', 'Admin\StudentsActivitiesController');

    /* Manage Attendance */
    Route::resource('attendances', 'Admin\AttendancesController');

    /* Manage Student Attendances */
    Route::resource('students.attendances', 'Admin\StudentsAttendancesController', ['only' => [
        'index',
    ]]);

    /* Manage Pick Up */
    Route::resource('pick_ups', 'Admin\PickUpsController', [
        'only' => ['index', 'show']
    ]);

    /* Manage Restore Students */
    Route::get('/restoreStudents', 'Admin\RestoreStudentsController@index')->name('restorestudents.index');
    Route::get('/restoreStudents/{student}', 'Admin\RestoreStudentsController@show')->name('restorestudents.show');
    Route::post('/restoreStudents/{student}/restore', 'Admin\RestoreStudentsController@restore')->name('restorestudents.restore');

    /* Manage Role */
    Route::resource('roles', 'Admin\RolesController', [
        'only' => ['index', 'show', 'edit', 'update',]
    ]);

    /* Dashboard */
    Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');
});
