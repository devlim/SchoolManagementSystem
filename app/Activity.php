<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Activity extends Model
{
    protected $guarded = [
        'id',
        'student_id',
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'timeline',
        'log',
    ]; 

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'timeline',
    ];

    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    public static function getRules()
    {
        return [
            'timeline' => 'required|date_format: "Y-m-d H:i:s"',
            'log' => 'required|min:5|max:15000' // max: 65,535
        ];
    }

    public static function getCustomMessage()
    {
        return [
            'timeline.required' => 'Activity date is required.',
            'timeline.date_format' => 'Activity is in invalid date format.',
            'log.required' => 'Activity log is required.',
            'log.max' => 'Activity log may not be greater than 15000 characters.',
        ];
    }

    public function setTimelineAttribute($value)
    {
        $this->attributes['timeline'] = Carbon::parse($value, config('app.app_sms.user_timezone'))->setTimezone('UTC');
    }

    public function getTimelineAttribute($value)
    {
        if ($value != null ) {
            return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
        }
    }
}
