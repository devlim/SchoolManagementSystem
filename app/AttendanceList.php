<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceList extends Model
{
    protected $touches = ['attendance'];

    protected $fillable = [
        'attend',
        'remark',
        'student_id'
    ];

    public function attendance()
    {
        return $this->belongsTo('App\Attendance');
    }

    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    /*
    * NOTE: student_id should not be manipulate via user throught system,
    * but in order to take advantange of request validation exists rules,
    * this must be defined for the mass assignment and request valitaion process.
    */
    public static function getRules()
    {
        return [
            'attend' => 'nullable|boolean',
            'remark' => 'nullable|max:255',
            'student_id' => 'exists:students,id'
        ];
    }
}
