<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $fillable = [
        'date',
        'remark',
    ];

    public function list()
    {
        return $this->hasMany('App\AttendanceList');
    }

    public static function getRules()
    {
        return [
            'date' => 'required|date_format:"Y-m-d"|',
            'remark' => 'nullable|max:255'
        ];
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }
}
