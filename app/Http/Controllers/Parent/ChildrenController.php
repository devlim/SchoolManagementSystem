<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use Carbon\Carbon;
use App\AttendanceList;

class ChildrenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student.status');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $student = Auth::user()->student;

        return view('parent.children.profile', compact('student'));
    }

    public function activities(Request $request)
    {
        $today_date = Carbon::today()->toDateString();
        $filter_date_meta = $this->getFilterDate($request);
        $filter_date = $filter_date_meta['filter_date'];

        $student = Auth::user()->student;
        $activities = Auth::user()->student->activities()->whereBetween(
            'timeline',
            [
                $filter_date_meta['startOfDay'],
                $filter_date_meta['endOfDay'],
            ]
        )->orderBy('timeline', 'desc')->get();

        return view('parent.children.activities', compact('student', 'activities', 'filter_date'));
    }

    public function attendances(Request $request)
    {
        $filter_date_meta = $this->getFilterMonth($request);

        $attendances = AttendanceList::select('attendance_lists.attend', 'attendances.date')
            ->join('attendances', 'attendances.id', '=', 'attendance_lists.attendance_id')
            ->where('attendance_lists.student_id', Auth::user()->student->id)
            ->whereBetween(
                'attendances.date',
                [
                    $filter_date_meta['startOfMonth'],
                    $filter_date_meta['endOfMonth'],
                ]
            )
            ->orderBy('attendances.date', 'DESC')
            ->orderBy('attendances.created_at', 'DESC')
            ->get();

        return view('parent.children.attendances', compact('attendances'));
    }

    private function getFilterDate(Request $request)
    {
        if ($request->has('filter_date') &&
            isset($request->filter_date)) {
            $validate = $this->validate($request, [
                'filter_date' => 'date',
            ]);
            $date = Carbon::parse($validate['filter_date']);
        } else {
            $date = Carbon::today();
        }

        $startOfDay = $date->toDateTimeString();
        $endOfDay = $date->endOfDay()->toDateTimeString();
        $filter_date = $date->toDateString();

        return compact('startOfDay', 'endOfDay', 'filter_date');
    }

    private function getFilterMonth(Request $request)
    {
        if ($request->has('filter_date') &&
            isset($request->filter_date)) {
            $validate = $this->validate(
                $request,
                [
                    'filter_date' => 'date_format:"m-Y"',
                ],
                [
                    'filter_date.date_format' => 'The filter attendance date is in invalid date format.',
                ]
            );

            $filter_date = explode('-', $request->input('filter_date'));
            $month = $filter_date[0];
            $year = $filter_date[1];

            $date = Carbon::create($year, $month);
            $startOfMonth = $date->startOfMonth()->toDateString();
            $endOfMonth = $date->endOfMonth()->toDateString();
        } else {
            $date = Carbon::today();
            $startOfMonth = $date->startOfMonth()->toDateString();
            $endOfMonth = $date->endOfMonth()->toDateString();
        }

        return compact('startOfMonth', 'endOfMonth');
    }
}
