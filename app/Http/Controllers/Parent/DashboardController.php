<?php

namespace App\Http\Controllers\Parent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Student;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student.status')->except('inactive');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today_date = Carbon::today()->toDateString();
        $today_activities = Auth::user()->student->today_activities;

        return view('parent.dashboard.index', compact('today_date', 'today_activities'));
    }

    public function inactive()
    {
        $student = Student::onlyTrashed()
            ->where('user_id', '=', Auth::user()->id)
            ->firstOrFail();

        return view('parent.dashboard.inactive', compact('student'));
    }
}
