<?php
/*
- Only one active pick up form is allowed per days,
- Pick up form is not allowed to be edited or deleted.
- Only need methods of:
    - history - history or active pick up form
    - show - view single pick up form in detail
    - create
    - store
    - cancel
*/
namespace App\Http\Controllers\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentPickUpRequest;

use App\PickUp;
use Auth;
use Carbon\Carbon;

class PickUpsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('student.status');
    }

    public function show(Request $request)
    {
        $pick_up = PickUp::where('student_id', Auth::user()->student->id)
            ->where('id', $request->pick_up)
            ->firstOrFail();

        return view('parent.pick_up.show', compact('pick_up'));
    }

    public function create()
    {
        $today_date = Carbon::today()->toDateString();

        $today_pick_ups = PickUp::where('student_id', Auth::user()->student->id)
            ->where('submit_date', $today_date)
            ->orderBy('created_at', 'DESC')
            ->get();

        $active_today_pick_up = $today_pick_ups
            ->where('status', 'active')
            ->all();

        $pick_up = new PickUp;
        $pick_up->submit_date = $today_date;

        return view('parent.pick_up.create',
            compact(
                'today_pick_ups',
                'active_today_pick_up',
                'pick_up',
                'today_date'
            )
        );
    }

    public function store(StoreStudentPickUpRequest $request)
    {
        $active_today_pick_up = PickUp::where('student_id', Auth::user()->student->id)
            ->where('submit_date', Carbon::today()->toDateString())
            ->where('status', 'active')
            ->count();

        if ($active_today_pick_up > 0) {
            abort(404); // only one active pick up form is allowed per day
        }

        $pick_up = new PickUp;
        $pick_up->fill($request->all());
        $pick_up->submit_date = Carbon::today()->toDateString();
        Auth::user()->student->pick_ups()->save($pick_up);

        flash('Successfully submit pick up form.')->success();

        return redirect()->route('parent.pick_ups.create');
    }

    public function cancel(Request $request)
    {
        $pick_up = PickUp::where('student_id', Auth::user()->student->id)
            ->findOrFail($request->id);
        $pick_up->status = 'cancel';
        $pick_up->cancel_at = Carbon::now()->toDateTimeString();
        $pick_up->save();

        flash('Successfully cancel selected pick up form.')->success();

        return redirect()->route('parent.pick_ups.create');
    }

    /*
    * List all pick up forms (filter by year)
    * NOTE: filter by year will be implement later
    */
    public function history()
    {
        //$year =  Carbon::now()->year;
        $pick_ups = PickUp::where('student_id', Auth::user()->student->id)
            //->whereYear('submit_date', '=', $year)
            ->orderBy('submit_date', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view(
            'parent.pick_up.history',
            compact('pick_ups')
        );
    }
}
