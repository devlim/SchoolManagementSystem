<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Activity;
use App\Http\Requests\StoreStudentActivityRequest;

use Carbon\Carbon;
use DB;

class StudentsActivitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin.has.permissions');
    }

    public function index(Student $student, Request $request)
    {

        $filter_date_meta = $this->getFilterDate($request);
        $filter_date = $filter_date_meta['filter_date'];
        $activities = $student->activities()->whereBetween(
            'timeline',
            [
                $filter_date_meta['startOfDay'],
                $filter_date_meta['endOfDay'],
            ]
        )->orderBy('timeline', 'desc')->get();

        return view('admin.students_activities.index', compact('student', 'activities', 'filter_date'));
    }

    public function show(Student $student, Request $request)
    {
        // Do not provide single activity page, it does not make sense
        return redirect()->route('students.activities.index', $student->id);
    }

    public function create(Student $student)
    {
        $activity = new Activity;

        return view('admin.students_activities.create', compact('student', 'activity'));
    }

    public function store(Student $student, StoreStudentActivityRequest $request)
    {
        $student->activities()->create($request->all());

        $filter_date = Carbon::parse($request->timeline)->format('Y-m-d');

        flash('Successfully save activity record.')->success();

        return redirect()->route(
            'students.activities.index',
            [
                'student' => $student,
                'filter_date' => $filter_date,
            ]
        );
    }

    public function edit(Student $student, Activity $activity)
    {
        return view('admin.students_activities.edit', compact('student', 'activity'));
    }

    public function update(Student $student, Activity $activity, StoreStudentActivityRequest $request)
    {
        $activity->fill($request->all())->save();

        $filter_date = Carbon::parse($request->timeline)->format('Y-m-d');

        flash('Successfully update activity record.')->success();

        return redirect()->route('students.activities.index', [
            'student' => $student,
            'filter_date' => $filter_date,
        ]);
    }

    public function destroy(Student $student, Activity $activity)
    {
        $filter_date = Carbon::parse($activity->timeline)->format('Y-m-d');

        $activity->delete();

        flash('Successfully delete activity record.')->success();

        return redirect()->route('students.activities.index', [
            'student' => $student,
            'filter_date' => $filter_date,
        ]);
    }

    private function getFilterDate(Request $request)
    {
        if ($request->has('filter_date')) {
            $validate = $this->validate($request, [
                'filter_date' => 'date',
            ]);
            $date = Carbon::parse($validate['filter_date']);
        } else {
            $date = Carbon::today();
        }

        $startOfDay = $date->toDateTimeString();
        $endOfDay = $date->endOfDay()->toDateTimeString();
        $filter_date = $date->toDateString();

        return compact('startOfDay', 'endOfDay', 'filter_date');
    }
}
