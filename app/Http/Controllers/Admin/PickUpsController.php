<?php
/*
* CURD
* - Cannot create (parent right)
* - Cannot Edit (nobody has right)
* - Cannot deleted and void  (parent right)
* - Can read
*/
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PickUp;
use Carbon\Carbon;

class PickUpsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin.has.permissions');
    }

    public function index(Request $request)
    {
        $filter_date_meta = $this->getFilterDate($request);
        $filter_date = $filter_date_meta['filter_date'];
        $pick_ups = PickUp::with('student')->has('student')->whereBetween(
            'submit_date',
            [
                $filter_date_meta['startOfDay'],
                $filter_date_meta['endOfDay'],
            ]
        )
        ->orderBy('submit_date', 'DESC')
        ->orderBy('created_at', 'DESC')
        ->get();

        return view('admin.pick_ups.index', compact('pick_ups'));
    }

    public function show(PickUp $pick_up)
    {
        return view('admin.pick_ups.show', compact('pick_up'));
    }

    private function getFilterDate(Request $request)
    {
        if ($request->has('filter_date')) {
            $validate = $this->validate($request, [
                'filter_date' => 'date',
            ]);
            $date = Carbon::parse($validate['filter_date']);
        } else {
            $date = Carbon::today();
        }

        $startOfDay = $date->toDateTimeString();
        $endOfDay = $date->endOfDay()->toDateTimeString();
        $filter_date = $date->toDateString();

        return compact('startOfDay', 'endOfDay', 'filter_date');
    }
}
