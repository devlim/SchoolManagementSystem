<?php
/*
- index/listing: viewing list of generate attendance based on date
    - filter by month/year
- view: viewing attendance list
- create: generate attendance entry
- update: edit atendance listing
    - cannot edit attendance date
    - only can edit attendance remark
*/

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Carbon\Carbon;
use App\Attendance;
use App\AttendanceList;
use App\Student;

class AttendancesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin.has.permissions');
    }

    public function index()
    {
        $attendances = Attendance::orderBy('date', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();

        return view('admin.attendances.index', compact('attendances'));
    }

    public function create()
    {
        $attendance = new Attendance;

        $students = Student::all();
        $attendent_lists = [];
        foreach ($students as $student) {
            $attendanceList = new AttendanceList;
            $attendanceList->student = $student;
            $attendent_lists[] = $attendanceList;
        }
        $attendance->list = $attendent_lists;

        return view('admin.attendances.create', compact('attendance'));
    }

    public function store(Request $request)
    {
        /*
        * Validate both Attendance and AttendanceList model.
        * refer to getModelsRules & getModelsCustomMessage methods.
        */
        $validator = Validator::make(
            $request->all(),
            $this->getModelsRules(),
            $this->getModelsCustomMessage()
        )
        ->after(function ($validator) use ($request) {
            /* Validate - attendance required at least one student */
            if ($request->filled('AttendanceList') == false) {
                $validator
                    ->errors()
                    ->add('empty_students_list', 'Attendance required at least one student');
            }
            /* Validate - prevent save deleted students (malicious actions) */
            if ($request->filled('AttendanceList') == true) {
                $student_ids = $request->input('AttendanceList.*.student_id');
                $deleted_student = Student::onlyTrashed()
                    ->whereIn('id', $student_ids)->get();
                if ($deleted_student->isEmpty() == false) {
                    $validator
                        ->errors()
                        ->add('deleted_students_list', 'There is an attempt of suspected action.');
                }
            }
        })
        ->validate();

        $attendance = Attendance::create($request->input('Attendance'));
        $attendanceList = [];

        foreach ($request->input('AttendanceList') as $item) {
            $attendanceList[] = new AttendanceList($item);
        }

        $attendance->list()->saveMany($attendanceList);

        flash('Successfully save attendance record.')->success();

        return redirect()->route('attendances.show', $attendance->id);
    }

    public function show(Request $request)
    {
        $attendance = Attendance::with('list')->with(['list.student' => function ($q) {
            $q->withTrashed();
        }])->where('id', $request->attendance)->firstOrFail();

        return view('admin.attendances.show', compact('attendance'));
    }

    public function edit(Request $request)
    {
        $attendance = Attendance::with('list')->with(['list.student' => function ($q) {
            $q->withTrashed();
        }])->where('id', $request->attendance)->firstOrFail();

        return view('admin.attendances.edit', compact('attendance'));
    }

    public function update(Request $request)
    {
        /*
        * Validate both Attendance and AttendanceList model.
        * refer to getModelsRules & getModelsCustomMessage methods.
        */
        $validator = Validator::make(
            $request->all(),
            $this->getModelsRules(),
            $this->getModelsCustomMessage()
        )
        ->after(function ($validator) use ($request) {
            /* Validate - attendance required at least one student */
            if ($request->filled('AttendanceList') == false) {
                $validator
                    ->errors()
                    ->add('empty_students_list', 'Attendance required at least one student');
            }
            /* Validate - prevent save deleted students (malicious actions) */
            /* Before a student deleted, it do appear on attendance list, If a student been deleted, it need to display */
            /*if ($request->filled('AttendanceList') == true) {
                $student_ids = $request->input('AttendanceList.*.student_id');
                $deleted_student = Student::onlyTrashed()
                    ->whereIn('id', $student_ids)->get();
                if ($deleted_student->isEmpty() == false) {
                    $validator
                        ->errors()
                        ->add('deleted_students_list', 'There is an attempt of suspected action.');
                }
            }*/
        })
        ->validate();

        $attendance = Attendance::with('list')->where('id', $request->attendance)->firstOrFail();
        $attendance->fill([
            'date' => $request->input('Attendance.date'),
            'remark' => $request->input('Attendance.remark'),
        ]);

        foreach ($attendance->list as $i => $attendent_list) {
            $attendent_list->fill([
                'attend' => $request->input('AttendanceList.' . $i . '.attend')
            ]);
            $attendent_list->save();
        }

        $attendance->save();

        flash('Successfully updated  attendance')->success();

        return redirect()->route('attendances.show', $attendance->id);
    }

    public function destroy(Attendance $attendance)
    {
        $attendance->delete();

        flash('Successfully delete attendance')->success();

        return redirect()->route('attendances.index');
    }

    private function getModelsRules()
    {
        return [
            'Attendance.date' => 'required|date_format:"Y-m-d"|',
            'Attendance.remark' => 'nullable|max:255',
            'AttendanceList.*.attend' => 'required|boolean',
            'AttendanceList.*.student_id' => 'exists:students,id',
            'AttendanceList.*.student_id' => 'distinct',
        ];
    }

    private function getModelsCustomMessage()
    {
        return [
            'Attendance.date.required' => 'The attendance date field is required.',
            'Attendance.date.date_format' => 'The attendance date is in invalid date format.',
            'Attendance.remark.max' => 'The remark may not be greater than 255 characters.',
            'AttendanceList.*.attend.boolean' => 'The attend checkbox field is in invalid format.',
            'AttendanceList.*.student_id.exists' => 'The student is not exist.',
            'AttendanceList.*.student_id.distinct' => 'There is attempts to save duplicate student in a attendance (:attributes)',
        ];
    }
}
