<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

use App\User;
use App\Student;
use App\Mail\NotifyNewParentAccountLoginDetail;
use App\Http\Requests\StoreStudentRequest;

class StudentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin.has.permissions');
    }

    public function index()
    {

        $model = Student::all();

        return view('admin.students.index', compact('model'));
    }

    public function show(Student $student)
    {
        return view('admin.students.show', compact('student'));
    }

    public function create()
    {
        $student = new Student;
        $user = new User;

        return view('admin.students.create', compact('student', 'user'));
    }

    public function store(StoreStudentRequest $request)
    {
        $login_password = str_random(5);
        $user = User::create([
            'username' => $request->username,
            'profile_name' => $request->profile_name,
            'email' => $request->email,
            'password' => Hash::make($login_password),
        ]);
        $student = $user->student()->create($request->except('username', 'profile_name', 'email', 'password'));
        //$student = Student::create($request->all());

        \Mail::to($user)->send(new NotifyNewParentAccountLoginDetail($user, $login_password));

        flash('Successfully create student.')->success();

        //return redirect()->route('students.index',);
        return redirect()->route('students.show', $student->id);
    }

    public function edit(Student $student)
    {
        return view('admin.students.edit', compact('student'));
    }

    public function update(Student $student, Request $request)
    {
        $rules = Student::getRules();

        if (strcasecmp($student->code, $request->code) === 0) { //Mysql case-insensetive by default
            $rules['code'] = 'required|max:10';
        }

        $this->validate($request, $rules, Student::getCustomMessage());
        $student->fill($request->input())->save();

        flash('Successfully update')->success();

        return redirect()->route('students.show', $student->id);
    }

    public function destroy(Student $student)
    {
        $user = $student->user;
        $student_name = $student->name;
        Student::destroy($student->id);
        flash('Successfully delete student: ' . $student_name)->success();

        return redirect()->route('students.index');
    }
}
