<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\AttendanceList;
use Carbon\Carbon;

class StudentsAttendancesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin.has.permissions');
    }

    public function index(Student $student, Request $request)
    {
        $filter_date_meta = $this->getFilterMonth($request);

        $attendances = AttendanceList::select('attendance_lists.attend', 'attendances.date', 'attendances.id')
            ->join('attendances', 'attendances.id', '=', 'attendance_lists.attendance_id')
            ->where('attendance_lists.student_id', $student->id)
            ->whereBetween(
                'attendances.date',
                [
                    $filter_date_meta['startOfMonth'],
                    $filter_date_meta['endOfMonth'],
                ]
            )
            ->orderBy('attendances.date', 'DESC')
            ->orderBy('attendances.created_at', 'DESC')
            ->get();

        return view('admin.students_attendances.index', compact('student', 'attendances'));
    }

    private function getFilterMonth(Request $request)
    {
        if ($request->has('filter_date') &&
            isset($request->filter_date)) {
            $validate = $this->validate(
                $request,
                [
                    'filter_date' => 'date_format:"m-Y"',
                ],
                [
                    'filter_date.date_format' => 'The filter attendance date is in invalid date format.',
                ]
            );

            $filter_date = explode('-', $request->input('filter_date'));
            $month = $filter_date[0];
            $year = $filter_date[1];

            $date = Carbon::create($year, $month);
            $startOfMonth = $date->startOfMonth()->toDateString();
            $endOfMonth = $date->endOfMonth()->toDateString();
        } else {
            $date = Carbon::today();
            $startOfMonth = $date->startOfMonth()->toDateString();
            $endOfMonth = $date->endOfMonth()->toDateString();
        }

        return compact('startOfMonth', 'endOfMonth');
    }
}
