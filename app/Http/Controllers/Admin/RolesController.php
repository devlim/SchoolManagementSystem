<?php
/*
* 2018-03-17, at the moment, does not allow create and deleted
*/
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('is.superadmin');
        $this->middleware('denied.superadmin.edit.own.permissions')->only('edit');
        $this->middleware('admin.has.permissions');
    }

    public function index()
    {
        $roles = Role::all();

        return view('admin.roles.index', compact('roles'));
    }

    public function show(Role $role)
    {
        $permission_labels = Permission::getLabels();

        return view('admin.roles.show', compact('role', 'permission_labels'));
    }

    public function edit(Role $role)
    {
        $permission_labels = Permission::getLabels();

        return view('admin.roles.edit', compact('role', 'permission_labels'));
    }

    public function update(Role $role, Request $request)
    {
        $permissions = $role->permissions;

        $validator = Validator::make(
            $request->all(),
            $this->getModelsRules()
        )->after(function ($validator) use ($request, $permissions) {
            if (count($request->input('Permission.*.enabled')) != $permissions->count()) {
                $validator
                    ->errors()
                    ->add('input_size_not_match_db_rows_size', 'There is an attempt of suspected action.');
            }
        })
        ->validate();

        foreach ($permissions as $i => $permission) {
            $permission->enabled = $request->input('Permission.' . $i . '.enabled');
            $permission->save();
        }

        flash('Successfully updated  roles')->success();

        return redirect()->route('roles.show', $role->id);
    }

    public function getModelsRules()
    {
        return [
            'Permission.*.enabled' => 'required|boolean',
        ];
    }
}
