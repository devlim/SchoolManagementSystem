<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Student;

class RestoreStudentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->middleware('admin.has.permissions');
    }

    public function index()
    {
        $students = Student::onlyTrashed()->get();

        return view('admin.restore_students.index', compact('students'));
    }

    public function show(Request $request)
    {
        $student = Student::onlyTrashed()
            ->where('id', '=', $request->student)
            ->firstOrFail();

        return view('admin.restore_students.show', compact('student'));
    }

    public function restore(Request $request)
    {
        $student = Student::onlyTrashed()
            ->where('id', '=', $request->student)
            ->firstOrFail();

        $student->restore();

        flash('Successfully restore student.')->success();

        return redirect()->route('students.show', $student->id);
    }
}
