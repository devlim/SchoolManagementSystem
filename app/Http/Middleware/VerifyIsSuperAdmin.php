<?php

namespace App\Http\Middleware;

use Closure;

class VerifyIsSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //1. Only one Super Admin is allow
        //2. Super Admin id is 1
        if ($request->user()->id != 1) {
            flash('Only Super Admin is allow to edit roles and roles permissions')->error();
            return redirect()->route('admin.dashboard');
        }

        return $next($request);
    }
}
