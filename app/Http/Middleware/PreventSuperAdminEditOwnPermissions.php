<?php

namespace App\Http\Middleware;

use Closure;

class PreventSuperAdminEditOwnPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Super admin is not allowed to edit it own permissions (admins table, id: 1), (roles table, id: 1)
        // and in general sense, it has all the permissions
        if ($request->user()->id == 1 &&
            $request->route()->parameters()['role']->id == 1) {
            flash('For safety purpose and general sense, <br />Super Admin is not allow to edit it own permissions.')->warning();
            return redirect()->back();
        }
        return $next($request);
    }
}
