<?php

namespace App\Http\Middleware;

use Closure;

class VerifyAdminPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route_to_permissions = [
            'students.index' => 'students.index',
            'students.create' => 'students.create',
            'students.store' => 'students.create',
            'students.edit' => 'students.edit',
            'students.update' => 'students.edit',
            'students.show' => 'students.show',
            'students.destroy' => 'students.destroy',
            'students.attendances.index' => 'students.attendances.index',
            'restorestudents.index' => 'restorestudents.index',
            'restorestudents.show' => 'restorestudents.show',
            'restorestudents.restore' => 'restorestudents.restore',
            'attendances.index' => 'attendances.index',
            'attendances.create' => 'attendances.create',
            'attendances.store' => 'attendances.create',
            'attendances.edit' => 'attendances.edit',
            'attendances.update' => 'attendances.edit',
            'attendances.show' => 'attendances.show',
            'attendances.destroy' => 'attendances.destroy',
            'pick_ups.index' => 'pick_ups.index',
            'pick_ups.show' => 'pick_ups.show',
            'students.activities.index' => 'students.activities.index',
            'students.activities.create' => 'students.activities.create',
            'students.activities.store' => 'students.activities.create',
            'students.activities.edit' => 'students.activities.edit',
            'students.activities.update' => 'students.activities.edit',
            'students.activities.show' => 'students.activities.show',
            'students.activities.destroy' => 'students.activities.destroy',
            'roles.index' => 'roles.index',
            'roles.edit' => 'roles.edit',
            'roles.update' => 'roles.edit',
            'roles.show' => 'roles.show',
        ];

        $route_permission = $route_to_permissions[$request->route()->getName()];

        if ($request->session()->get('permissions')[$route_permission] == false) {
            flash('You do not has the access right')->error();
            return response()->view('admin.errors.403');
        }

        return $next($request);
    }
}
