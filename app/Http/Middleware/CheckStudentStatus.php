<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckStudentStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->student == null) {
            return redirect()->route('parent.inactive');
        }
        return $next($request);
    }
}
