<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Activity;
use Auth;

class StoreStudentActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::guard('admin')->check() == 1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return Activity::getRules();
     }

     public function messages()
     {
         return Activity::getCustomMessage();
     }
}
