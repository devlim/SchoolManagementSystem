<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\PickUp;
use Auth;

class StoreStudentPickUpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (Auth::guard('web')->check() == 1);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return PickUp::getRules();
    }
}
