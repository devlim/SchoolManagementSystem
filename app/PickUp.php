<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickUp extends Model
{
    /*
    * prevent laravel try to set updated_at column value,
    * and throw errors
    */
    const UPDATED_AT = null;

    /*
    * define column(s) default value
    */
    protected $attributes = [
        'status' => 'active',
    ];

    protected $fillable = [
        'submit_date',
        'driver_name',
        'vehicle_plate_num',
        'relationship',
        'comment',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'cancel_at',
    ];

    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    public static function getStatusRange()
    {
        return [
            'active',
            'cancel',
        ];
    }

    public static function getRules()
    {
        return [
            'driver_name' => 'required|max:40',
            'vehicle_plate_num' => 'required|max:20',
            'relationship' => 'required|max:20',
            'comment' => 'nullable|max:255',
        ];
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }

    public function getCancelAtAttribute($value)
    {
        if ($value != null) {
            return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
        }
    }
}
