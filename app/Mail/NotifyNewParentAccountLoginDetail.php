<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class NotifyNewParentAccountLoginDetail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $login_password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $login_password)
    {
        $this->user = $user;
        $this->login_password = $login_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.notify-new-parent-account-login-detail');
    }
}
