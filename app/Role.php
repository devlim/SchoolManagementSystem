<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
    ];

    protected static function boot()
    {
        static::creating(function ($model) {
            $model->code = str_slug($model->name, '-');
        });
    }

    public function permissions()
    {
        return $this->hasMany('App\Permission');
    }

    //refer to Admin model
    public function user()
    {
        return $this->hasMany('App\Admin');
    }

    public static function getRules()
    {
        return [
            'name' => 'required|regex:/^[a-zA-Z]+$/u|max:30',
        ];
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }
}
