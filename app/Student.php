<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class Student extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'name',
        'dob',
        'dop',
        'language',
        'race',
        'gender',
        'type',
        'registered',
        'father_name',
        'father_contact',
        'mother_name',
        'mother_contact',
        'address1',
        'address2',
        'remarks1',
        'remarks2',
        'remarks3',
        'status',
    ];

    protected $guarded = [
        'id',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'registered',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function pick_ups()
    {
        return $this->hasMany('App\PickUp');
    }

    public function attendances()
    {
        return $this->hasMany('App\AttendanceList');
    }

    public function getTodayActivitiesAttribute()
    {
        $today_date = Carbon::today()->toDateString();
        return $this->activities()
            ->where('timeline', 'LIKE', '%' . $today_date . '%')
            ->orderBy('timeline', 'DESC')
            ->get();
    }

    public static function getStatusRange()
    {
        return [
            'Active',
            'Non-Active',
            'Achieved',
        ];
    }

    public static function getGenderRange()
    {
        return [
            'Male',
            'Female',
        ];
    }

    public static function getRules()
    {
        $status_range = 'required|in:' . implode(",", self::getStatusRange());
        $gender_range = 'max:10|required|in:' . implode(",", self::getGenderRange());

        return [
            'code' => 'required|max:10|unique:students',
            'name' => 'required|max:40',
            'dob' => 'required|date_format:"Y-m-d"',
            'dop' => 'required|max:20',
            'language' => 'nullable|max:20',
            'race' => 'required|max:15',
            'gender' => $gender_range,
            'type' => 'nullable|max:10',
            'registered' => 'required|date_format: "Y-m-d H:i:s"',
            'father_name' => 'required|max:40',
            'father_contact' => 'required|numeric|digits_between:1,30',
            'mother_name' => 'required|max:40',
            'mother_contact' => 'required|numeric|digits_between:1,30',
            'address1' => 'required|max:100',
            'address2' => 'nullable|max:100',
            'remarks1' => 'nullable|max:120',
            'remarks2' => 'nullable|max:120',
            'remarks3' => 'nullable|max:120',
            'status' => $status_range,
        ];
    }

    public static function getCustomMessage()
    {
        return [
            'dop.required' => 'The place of birth field is required.',
            'dop.max' => 'Place of birth may not be greater than 20 characters.',
            'father_contact.max' => 'Father contact no. may not be greater than 30 characters.',
            'mother_contact.max' => 'Mother contact no. may not be greater than 30 characters.',
            'remarks1.max' => 'Health status may not be greater than 120 characters.',
            'remarks2.max' => 'Academic may not be greater than 120 characters.',
            'remarks3.max'=> 'Special remarks may not be greater than 120 characters.',
            'address1.max' => 'Address 1 may not be greater than 120 characters.',
            'address1.required' => 'The address 1 field is required.',
            'address2.max' => 'Address 2 may not be greater than 120 characters.',
            'dob.required' => 'The date of birth field is required.',
            'dob.date_format' => 'Date of birth is in invalid date format.',
            'registered.date_format' => 'Registered date is in invalid date format.',
            'registered.required' => 'The registered date field is required.'
        ];
    }

    public function setRegisteredAttribute($value)
    {
        $this->attributes['registered'] = Carbon::parse($value, config('app.app_sms.user_timezone'))->setTimezone('UTC');
    }

    public function getRegisteredAttribute($value)
    {
        if ($value != null ) {
            return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
        }
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }

    public function getUpdatedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }

    public function getDeletedAtAttribute($value)
    {
        return $this->asDateTime($value)->tz(config('app.app_sms.user_timezone'));
    }
}
