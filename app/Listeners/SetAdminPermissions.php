<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Auth;

class SetAdminPermissions
{
    public $request;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Set Admin permissions ability
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        if (Auth::guard('admin')->check() == true) {
            $permissions = array_column($event->user->role->permissions->toArray(), 'enabled', 'ability');
            $this->request->session()->put('permissions', $permissions);
        }
    }
}
