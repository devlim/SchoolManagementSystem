<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /*
    * prevent laravel try to set updated_at column value,
    * and throw errors
    */
    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $touches = ['role'];

    protected $fillable = [
        'enabled',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public static function getRules()
    {
        return [
            'ability' => 'required|max:50|unique:permissions',
            'enabled' => 'required|boolean',
        ];
    }

    public static function getLabels()
    {
        return [
            'students.index' => 'Students - Listing',
            'students.create' => 'Students - Create',
            'students.edit'  => 'Students - Edit',
            'students.show' => 'Students - Detail',
            'students.destroy' => 'Students - Delete',
            'students.attendances.index' => 'Students Attendances - Listing',
            'restorestudents.index' => 'Students - Restore Students Listing',
            'restorestudents.show' => 'Students - Restore Student Detail',
            'restorestudents.restore' => 'Students - Restore Student',
            'attendances.index' => 'Attendances - Listing',
            'attendances.create' => 'Attendances - Create',
            'attendances.edit' => 'Attendances - Edit',
            'attendances.show' => 'Attendances - Detail',
            'attendances.destroy' => 'Attendances - Delete',
            'pick_ups.index' => 'Submitted Pick Up - Listing',
            'pick_ups.show' => 'Submitted Pick Up - Detail',
            'students.activities.index' => 'Students Activities -  Listing',
            'students.activities.create' => 'Students Activities - Create',
            'students.activities.edit' => 'Students Activities - Edit',
            'students.activities.show' => 'Students Activities - Detail',
            'students.activities.destroy' => 'Students Activities - Delete',
            'roles.index' => 'Roles - Listing',
            'roles.edit'  => 'Role - Edit',
            'roles.show'  => 'Roles - Detail',
        ];
    }

    public static function getPredefinedLists()
    {
        return [
            'students.index', //students.index
            'students.create', //students.create, students.store
            'students.edit', //students.edit, students.update
            'students.show', //students.show
            'students.destroy', //students.destroy
            'students.attendances.index', //students.attendances.index
            'restorestudents.index', //restorestudents.index
            'restorestudents.show', //restorestudents.show
            'restorestudents.restore', //restorestudents.restore
            'attendances.index', //attendances.index
            'attendances.create', //attendances.create, attendances.store
            'attendances.edit', //attendances.edit, attendances.update
            'attendances.show', //attendances.show
            'attendances.destroy', //attendances.destroy
            'pick_ups.index', //pick_ups.index
            'pick_ups.show', //pick_ups.show
            'students.activities.index', //students.activities.index
            'students.activities.create', //students.activities.create, students.activities.store
            'students.activities.edit', //students.activities.edit, students.activities.update
            'students.activities.show', //students.activities.show
            'students.activities.destroy', //students.activities.destroy
            'roles.index', //roles.index
            'roles.edit', //roles.edit, roles.update
            'roles.show', //roles.show
        ];
    }

    public function setUpdatedAt($value) {
        // Do nothing.
    }

    public function setCreatedAt($value) {
        // Do nothing.
    }
}
