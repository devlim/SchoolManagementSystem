<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        /* Create admin(s)/operator(s) user */
        $this->call([
            RolesTableSeeder::class,
            PermissionsTableSeeder::class,
            AdminsTableSeeder::class,
        ]);

        /* Create parent(s) user */
        /*
        $admin = new \App\User;
        $admin->username = 'demo';
        $admin->profile_name = 'Parent Demo';
        $admin->email = 'demo@outlook.com';
        $admin->password = Hash::make('demodemo');
        $admin->save();
        */

        /*Create 50 fake students*/
        //(new Faker\Generator)->seed("StudentSeed");
        //factory(App\Student::class, 50)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
