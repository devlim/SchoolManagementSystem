<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Create first admin(important, due to there no way to create a superadmin)*/
        $superadmin = Role::create([
            'name' => 'Super Admin',
        ]);

        /*Create additional user who roles is Admin & Operator*/
        /*Admin*/
        $admin = Role::create([
            'name' => 'Admin',
        ]);
        /*operator*/
        $operator = Role::create([
            'name' => 'Operator',
        ]);
    }
}
