<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\Role;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Create first admin(important, due to there no way to create a superadmin)*/
        $super_admin_role = Role::where('name', '=', 'Super Admin')->first();
        $superadmin = Admin::create([
            'name' => 'Super Admin Demo',
            'email' => 'demo@gmail.com',
            'role_id' => $super_admin_role->id,
            'password' => Hash::make('demodemo'),
        ]);

        /*Create additional user who roles is Admin & Operator*/
        /*Admin*/
        $admin_role = Role::where('name', '=', 'Admin')->first();
        $admin = Admin::create([
            'name' => 'Admin Demo',
            'email' => 'admin_demo@gmail.com',
            'role_id' => $admin_role->id,
            'password' => Hash::make('demodemo'),
        ]);
        /*operator*/
        $operator_role = Role::where('name', '=', 'Operator')->first();
        $operator = Admin::create([
            'name' => 'Operator Demo',
            'email' => 'admin_operator@gmail.com',
            'role_id' => $operator_role->id,
            'password' => Hash::make('demodemo'),
        ]);
    }
}
