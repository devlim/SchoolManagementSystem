<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;
use App\Admin;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Super Admin*/
        $permissions_list = [
            'students.index' => 1, //students.index
            'students.create' => 1, //students.create, students.store
            'students.edit' => 1, //students.edit, students.update
            'students.show' => 1, //students.show
            'students.destroy' => 1, //students.destroy
            'students.attendances.index' => 1, //students.attendances.index
            'restorestudents.index' => 1, //restorestudents.index
            'restorestudents.show' => 1, //restorestudents.show
            'restorestudents.restore' => 1, //restorestudents.restore
            'attendances.index' => 1, //attendances.index
            'attendances.create' => 1, //attendances.create, attendances.store
            'attendances.edit' => 1, //attendances.edit, attendances.update
            'attendances.show' => 1, //attendances.show
            'attendances.destroy' => 1, //attendances.destroy
            'pick_ups.index' => 1, //pick_ups.index
            'pick_ups.show' => 1, //pick_ups.show
            'students.activities.index' => 1, //students.activities.index
            'students.activities.create' => 1, //students.activities.create, students.activities.store
            'students.activities.edit' => 1, //students.activities.edit, students.activities.update
            'students.activities.show' => 1, //students.activities.show
            'students.activities.destroy' => 1, //students.activities.destroy
            'roles.index' => 1, //roles.index
            'roles.edit' => 1, //roles.edit, roles.update
            'roles.show' => 1, //roles.show
        ];
        $admin_role = Role::where('name', '=', 'Super Admin')->first();
        $admin_role_id = $admin_role->id;
        foreach ($permissions_list as $ability => $enabled) {
            Permission::create([
                'role_id' => $admin_role_id,
                'ability' => $ability,
                'enabled' => $enabled,
            ]);
        }

        /*Admin*/
        $permissions_list = [
            'students.index' => 1, //students.index
            'students.create' => 1, //students.create, students.store
            'students.edit' => 1, //students.edit, students.update
            'students.show' => 1, //students.show
            'students.destroy' => 1, //students.destroy
            'students.attendances.index' => 1, //students.attendances.index
            'restorestudents.index' => 1, //restorestudents.index
            'restorestudents.show' => 1, //restorestudents.show
            'restorestudents.restore' => 1, //restorestudents.restore
            'attendances.index' => 1, //attendances.index
            'attendances.create' => 1, //attendances.create, attendances.store
            'attendances.edit' => 1, //attendances.edit, attendances.update
            'attendances.show' => 1, //attendances.show
            'attendances.destroy' => 1, //attendances.destroy
            'pick_ups.index' => 1, //pick_ups.index
            'pick_ups.show' => 1, //pick_ups.show
            'students.activities.index' => 1, //students.activities.index
            'students.activities.create' => 1, //students.activities.create, students.activities.store
            'students.activities.edit' => 1, //students.activities.edit, students.activities.update
            'students.activities.show' => 1, //students.activities.show
            'students.activities.destroy' => 1, //students.activities.destroy
            'roles.index' => 0, //roles.index
            'roles.edit' => 0, //roles.edit, roles.update
            'roles.show' => 0, //roles.show
        ];
        $admin_role = Role::where('name', '=', 'Admin')->first();
        $admin_role_id = $admin_role->id;
        foreach ($permissions_list as $ability => $enabled) {
            Permission::create([
                'role_id' => $admin_role_id,
                'ability' => $ability,
                'enabled' => $enabled,
            ]);
        }

        /*Operator*/
        $permissions_list = [
            'students.index' => 1, //students.index
            'students.create' => 1, //students.create, students.store
            'students.edit' => 1, //students.edit, students.update
            'students.show' => 1, //students.show
            'students.destroy' => 1, //students.destroy
            'students.attendances.index' => 1, //students.attendances.index
            'restorestudents.index' => 1, //restorestudents.index
            'restorestudents.show' => 1, //restorestudents.show
            'restorestudents.restore' => 1, //restorestudents.restore
            'attendances.index' => 1, //attendances.index
            'attendances.create' => 1, //attendances.create, attendances.store
            'attendances.edit' => 1, //attendances.edit, attendances.update
            'attendances.show' => 1, //attendances.show
            'attendances.destroy' => 1, //attendances.destroy
            'pick_ups.index' => 1, //pick_ups.index
            'pick_ups.show' => 1, //pick_ups.show
            'students.activities.index' => 1, //students.activities.index
            'students.activities.create' => 1, //students.activities.create, students.activities.store
            'students.activities.edit' => 1, //students.activities.edit, students.activities.update
            'students.activities.show' => 1, //students.activities.show
            'students.activities.destroy' => 1, //students.activities.destroy
            'roles.index' => 0, //roles.index
            'roles.edit' => 0, //roles.edit, roles.update
            'roles.show' => 0, //roles.show
        ];
        $admin_role = Role::where('name', '=', 'Operator')->first();
        $admin_role_id = $admin_role->id;
        foreach ($permissions_list as $ability => $enabled) {
            Permission::create([
                'role_id' => $admin_role_id,
                'ability' => $ability,
                'enabled' => $enabled,
            ]);
        }
    }
}
