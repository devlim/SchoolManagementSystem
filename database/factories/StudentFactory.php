<?php

use Faker\Generator as Faker;

/* Predefined list */
$dop_list = [
    'Johor',
    'Kedah',
    'Kelantan',
    'Malacca',
    'Negeri Sembilan',
    'Pahang',
    'Penang',
    'Perak',
    'Perlis',
    'Selangor',
    'Terengganu',
    'Sabah',
    'Sarawak',
    'Kuala Lumpur',
    'Labuan',
    'Putrajaya',
];

$student_type = [
    'Stu Type 1',
    'Stu Type 2',
    'Stu Type 3',
];

$student_status = [
    'Active',
    'Non-Active',
    'Achieved',
];

$race = [
    'Chinese',
    'Malay',
    'India',
    'Foreign',
];

$gender = [
    'Male',
    'Female',
];
/* End of Predefined list */

$factory->define(App\Student::class, function (Faker $faker) use ($dop_list, $student_type, $student_status, $race, $gender) {
    return [
        'code' => str_random(10),
        'name' => $faker->name,
        'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'dop' => $faker->randomElement($dop_list),
        'language' => $faker->languageCode,
        'race' => $faker->randomElement($race),
        'gender' => $faker->randomElement($gender),
        'type' => $faker->randomElement($student_type),
        'registered' => $faker->dateTimeBetween($startDate = '-50 days', $endDate = '+10 days', $timezone = null),
        'father_name' => $faker->firstNameMale,
        'father_contact' => $faker->numerify('##########'),
        'mother_name' => $faker->firstNameFemale,
        'mother_contact' => $faker->numerify('###########'),
        'address1' => $faker->address,
        'address2' => '',
        'remarks1' => '',
        'remarks2' => '',
        'remarks3' => '',
        'status' => $faker->randomElement($student_status),
    ];
});
