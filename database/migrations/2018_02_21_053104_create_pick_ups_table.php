<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePickUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pick_ups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->date('submit_date', 'date_format:"d-m-Y"');
            $table->string('driver_name', 40);
            $table->string('vehicle_plate_num', 20);
            $table->string('relationship', 20);
            $table->string('comment', 255)->nullable();
            $table->string('status', 10);
            $table->timestamp('created_at')->nullable(); // only need created_at column
            $table->timestamp('cancel_at')->nullable(); // store parent cancel pick up form timestamp

            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pick_ups');
    }
}
