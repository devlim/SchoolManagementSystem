<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('code', 10)->unique();
            $table->string('name', 40);
            $table->date('dob');
            $table->string('dop', 20);
            $table->string('language', 20)->nullable();
            $table->string('race', 15);
            $table->string('gender', 10);
            $table->string('type', 10)->nullable();
            $table->datetime('registered');
            $table->string('father_name', 40);
            $table->string('father_contact', 30);
            $table->string('mother_name', 40);
            $table->string('mother_contact', 30);
            $table->string('address1', 100);    //Info: Residential addres
            $table->string('address2', 100)->nullable();    //Info: Offica Address
            $table->string('remarks1', 120)->nullable();    //Info: Student health status
            $table->string('remarks2', 120)->nullable();    //Info: Student academic records
            $table->string('remarks3', 120)->nullable();    //Info: Special remarks
            $table->string('status', 10);   //value consisnt of: Active, Non-Active, Achieved
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
