@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
          Generate Attendance
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-12">
                                @include('layouts.errors._form_errors_summary')
                            </div>
                            {!! Form::model($attendance, ['route' => 'attendances.store', 'method' => 'post']) !!}
                                @include('admin.attendances.inc._form')
                                <div class="col-md-12">
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover" role="grid" aria-describedby="student_listing">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th>Attendance</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if (count($attendance->list) != 0)
                                                            @foreach ($attendance->list as $i => $attendent_list)
                                                                <tr style="{{
                                                                    (
                                                                        count($errors->get('AttendanceList.' . $i . '.attend')) ||
                                                                        count($errors->get('AttendanceList.' . $i . '.student_id'))
                                                                    ) ? 'border-left: #dd4b39 5px solid;': '' }}">
                                                                    <td>{{ $i + 1 }}</td>
                                                                    <td>{{ $attendent_list->student->code }}</td>
                                                                    <td>{{ $attendent_list->student->name }}</td>
                                                                    <td>
                                                                        {{
                                                                            Form::checkbox(
                                                                                'attendance_list_attend[' . $i . ']',
                                                                                $attendent_list->attend,
                                                                                $attendent_list->attend,
                                                                                [
                                                                                    'data-num' => $i,
                                                                                    'class' => 'attendance_list_attend'
                                                                                ]
                                                                            )
                                                                        }}
                                                                        {{
                                                                            Form::hidden(
                                                                                'AttendanceList[' . $i . '][attend]',
                                                                                $attendent_list->attend? $attendent_list->attend : 0,
                                                                                [
                                                                                    'id' => 'attendance_list_' . $i
                                                                                ]
                                                                            )
                                                                        }}
                                                                        {{ Form::hidden('AttendanceList[' . $i . '][student_id]', $attendent_list->student->id) }}
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    {{
                                        Form::submit('Create', [
                                            'class' => 'btn btn-block btn-primary'
                                        ])
                                    }}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page-component')
<!-- Bootstrap Datepicker -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>

<script>
$(function () {
    jQuery(function () {
        jQuery('#datepicker').datepicker({
            autoclose: true,
            format: 'yyyy-mm-dd',
            todayBtn: 'linked'
        });

        jQuery('.attendance_list_attend').click(function (e) {
            var checked = $(this).prop('checked');
            var num = $(this).attr('data-num');

            var el = document.getElementById('attendance_list_' + num);
            if (checked) {
                el.value = 1;
            } else {
                el.value = 0;
            }
        });
    })();
});
</script>
@endsection
