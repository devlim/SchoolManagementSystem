@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
          View Attendance
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-12">
                                @include('layouts.errors._form_errors_summary')
                            </div>
                            <div class="form-group col-md-12">
                                {{
                                    Form::label('Attendance[date]', 'Attendance date', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                <p>
                                    {{ $attendance->date }}
                                </p>
                            </div>
                            <div class="form-group col-md-6">
                                {{
                                    Form::label('Attendance[created_at]', 'Attendance Created At', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                <p>
                                    {{ $attendance->created_at }}
                                </p>
                            </div>
                            <div class="form-group col-md-6">
                                {{
                                    Form::label('Attendance[updated_at]', 'Attendance Updated At', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                <p>
                                    {{ $attendance->updated_at }}
                                </p>
                            </div>
                            <div class="form-group col-md-12 count($errors->get('Attendance.remark')) ? 'has-error': '' }}">
                                {{
                                    Form::label('Attendance[remark]', 'Remark', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                <p>
                                    {{ $attendance->remark? $attendance->remark : '-' }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" role="grid" aria-describedby="student_listing">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                    <th>Attendance</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @if (count($attendance->list->isEmpty() != false))
                                                        @foreach ($attendance->list as $i => $attendent_list)
                                                            <tr>
                                                                <td>{{ $i + 1 }}</td>
                                                                <td>{{ $attendent_list->student->code }}</td>
                                                                <td>{{ $attendent_list->student->name }}</td>
                                                                <td>
                                                                    @if ($attendent_list->attend == true)
                                                                        <i class="fa fa-check" aria-hidden="true" style="color: #4CAF4F"></i>
                                                                    @else
                                                                        <i class="fa fa-times" aria-hidden="true" style="color: #f44336"></i>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <a href="#" id="deleteConfirmation" class="btn btn-block btn-danger" aria-label="Left Align">
                                    Delete
                                </a>
                            </div>
                            <div class="form-group col-md-6">
                                <a href="{{ route('attendances.edit', $attendance->id) }}" class="btn btn-block btn-primary" aria-label="Left Align">
                                    Edit
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            {!! Form::model($attendance, ['route' => ['attendances.destroy', $attendance->id], 'method' => 'delete', 'id' => 'deleteAttendance']) !!}
            {!! Form::close() !!}
        </div>
    </section>
@endsection


@section('page-component')
<script>
$(function () {
    $('#deleteConfirmation').click(function (e) {
        var msg = "Confirm deleting attendance?"
        var confirmation = confirm(msg);
        if (confirmation) {
          var attendanceForm = $('#deleteAttendance');
          attendanceForm.submit();
        } else {
            return false;
        }
    });
});
</script>
@endsection
