<div class="form-group col-md-12 {{ count($errors->get('Attendance.date')) ? 'has-error': '' }}">
    {{
        Form::label('Attendance[date]', 'Attendance date', [
            'class' => 'control-label'
        ])
    }}
    <div class="input-append date form_datetime">
        {{
            Form::text('Attendance[date]', $attendance->date, [
                'id' => 'datepicker',
                'class' => 'form-control',
                'placeholder' => 'Attendance date'
            ])
        }}
        <span class="add-on"><i class="icon-th"></i></span>
    </div>
</div>
<div class="form-group col-md-12 {{ count($errors->get('Attendance.remark')) ? 'has-error': '' }}">
    {{
        Form::label('Attendance[remark]', 'Remark', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::textarea('Attendance[remark]', $attendance->remark, [
            'class' => 'form-control',
            'placeholder' => 'Remark'
        ])
    }}
</div>
