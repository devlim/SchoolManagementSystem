@extends('layouts.app')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    @include('flash::message')
    <h1>
      Attendance Listing
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="attendance_listing">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Remark</th>
                                <th>View</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if (count($attendances) != 0)
                                    @foreach ($attendances as $i => $attendance)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $attendance->date }}</a></td>
                                            <td>{{ str_limit($attendance->remark, 17) }}</td>
                                            <td><a href="{{ route('attendances.show', $attendance->id) }}">View<a></td>
                                            <td><a href="{{ route('attendances.edit', $attendance->id) }}">Edit<a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
  jQuery(function () {
    jQuery('.dataTable').DataTable()
  })();
</script>
@endsection
