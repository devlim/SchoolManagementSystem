@extends('layouts.app')

@section('content')
<section class="content-header">
@include('flash::message')
    <h1>
    Operator Dashboard
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            @if (session('status'))
                <div class="alert alert-success">
                {{ session('status') }}
                </div>
            @endif
        <h3 class="box-title">Welcome</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            Welcome to operator dashboard!
            <br />
            You are logged in as Operator!
        </div>
    </div>
</section>
@endsection
