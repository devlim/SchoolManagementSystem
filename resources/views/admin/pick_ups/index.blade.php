@extends('layouts.app')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
    <h1>
      Pick Up Submitted Forms By Days
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @include(
                'layouts.form._filter_date',
                [
                    'header_title' => 'Filter pick up submitted forms by day',
                    'route_params' => 'pick_ups.index',
                    'filter_date_param' => 'filter_date',
                    'filter_date_param_placholder' => 'Filter pick up submitted forms by day',
                ]
            )
            <div class="box">
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="pick_ups_submitted_form_listing_by_month">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Student</th>
                                <th>Driver</th>
                                <th>Vechicle Plate Num</th>
                                <th>Relationship</th>
                                <th>Comment</th>
                                <th>Status</th>
                                <th>Cancel At</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if ($pick_ups->isEmpty() == false)
                                    @foreach ($pick_ups as $i => $pick_up)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $pick_up->student->name }}</td>
                                            <td>{{ $pick_up->driver_name }}</td>
                                            <td>{{ $pick_up->vehicle_plate_num }}</td>
                                            <td>{{ $pick_up->relationship }}</td>
                                            <td>{{ str_limit($pick_up->comment, 10) }}</td>
                                            <td>{{ $pick_up->status }}</td>
                                            <td>{{ $pick_up->cancel_at? $pick_up->cancel_at : '-'  }}</td>
                                            <td><a href="{{ route('pick_ups.show', $pick_up->id) }}">View<a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


@section('page-component')
<!-- Datepicker -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
  jQuery(function () {
    jQuery('.dataTable').DataTable();

    jQuery('#datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayBtn: 'linked'
    });
  })();
</script>
@endsection
