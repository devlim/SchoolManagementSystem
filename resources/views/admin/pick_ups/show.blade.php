@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
      Pick Up
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group col-md-6">
                        {{
                            Form::label('submit_date', 'Submit Date', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->submit_date }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('student_name', 'Student', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p><a href="{{ route('students.show', $pick_up->student->id )}}">{{ $pick_up->student->name }}</a></p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('driver_name', 'Driver Name', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->driver_name }}</p>
                    </div>

                    <div class="form-group col-md-6">
                        {{
                            Form::label('vehicle_plate_num', 'Vehicle Plate Num', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->vehicle_plate_num }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('relationship', 'Relationship', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->relationship }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('status', 'Status', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->status }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('created_at', 'Created at', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->created_at }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('cancel_at', 'Cancel at', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->cancel_at }}</p>
                    </div>
                    <div class="form-group col-md-12">
                        {{
                            Form::label('comment', 'Comment', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->comment? $pick_up->comment : '-' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
