<div class="form-group col-md-12 {{ count($errors->get('registered')) ? 'has-error': '' }}">
    {{
        Form::label('timeline', 'Activities Date', [
            'class' => 'control-label'
        ])
    }}
    <div class="input-append date form_datetime">
        {{
            Form::text('timeline', $activity->timeline, [
                'class' => 'form-control',
                'placeholder' => 'Activites date time'
            ])
        }}
        <span class="add-on"><i class="icon-th"></i></span>
    </div>
</div>
<div class="wysihtml5-textarea form-group col-md-12 {{ count($errors->get('remarks3')) ? 'has-error': '' }}">
    {{
        Form::label('log', 'Log', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::textarea('log', $activity->log, [
            'class' => 'form-control',
            'placeholder' => 'Activity log'
        ])
    }}
</div>

@section('page-component')
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script>
$(function () {
    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii:ss",
        autoclose: true,
        todayBtn: true,
        showMeridian: true
    });

    // Temp fix
    $('.icon-arrow-left').each(function () {
        $(this).attr('class', ' glyphicon glyphicon-arrow-left');
    });
    $('.icon-arrow-right').each(function () {
        $(this).attr('class', ' glyphicon glyphicon-arrow-right');
    });
});
</script>
@endsection
