@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
            Student Activities: {{ $student->name }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            @include('admin.students.inc._sub_menu')
            <div class="col-md-9">
                @include('admin.students_activities.inc._search')
                @if (count($activities) > 0)
                    <ul class="timeline">
                        <li class="time-label">
                            <span class="bg-red">
                                {{ $filter_date }}
                            </span>
                        </li>
                        @foreach ($activities as $i => $activity)
                            <li>
                            <i class="fa fa-life-ring bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time">
                                        <i class="fa fa-clock-o"></i> {{ $activity->timeline->format('h:i:s A') }}
                                    </span>
                                    <br/>
                                    <div class="timeline-body">
                                        {!! nl2br(e($activity->log)) !!}
                                    </div>
                                    <div class="timeline-footer">
                                        <a href="{{ route('students.activities.edit', [$student->id, $activity->id]) }}" class="btn btn-primary btn-xs" aria-label="Left Align">
                                            Edit
                                        </a>
                                        <div style="display: inline-block;">
                                            {!!
                                                Form::open(
                                                    [
                                                        'route' => [
                                                            'students.activities.destroy',
                                                            $student->id,
                                                            $activity->id,
                                                        ],
                                                        'method' => 'delete',
                                                    ]
                                                )
                                            !!}
                                                {{
                                                    Form::submit('Delete', [
                                                        'class' => 'btn btn-danger btn-xs deleteActivity',
                                                        'aria-label' => 'Left Align',
                                                    ])
                                                }}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                    </ul>
                @else
                <div class="box">
                    <div class="box-body">
                        There isn't any activies {{ $filter_date }}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('page-component')
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>

<script>
    $('#datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayBtn: 'linked'
    });

    $('.deleteActivity').click(function (e) {
        var confirmation = confirm("Are you sure you want to delete?");
        if (confirmation) {
            return true;
        } else {
            return false;
        }
    });
</script>
@endsection
