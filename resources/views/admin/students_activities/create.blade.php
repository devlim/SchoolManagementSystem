@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
            Create Student Activities: {{ $student->name }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            @include('admin.students.inc._sub_menu')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            @include('layouts.errors._form_errors_summary')
                        </div>
                        {!! Form::model($activity, ['route' => ['students.activities.store', $student->id], 'method' => 'post']) !!}
                            @include('admin.students_activities.inc._form')
                            <div class="form-group col-md-offset-6">
                                {{
                                    Form::submit('Create', [
                                        'class' => 'btn btn-block btn-primary'
                                    ])
                                }}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
