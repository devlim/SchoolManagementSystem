@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
          Restore Student
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group col-md-6">
                            {{
                                Form::label('code', 'Code', [
                                    'class' => 'form-required, control-label'
                                ])
                            }}
                            <p>{{ $student->code }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('name', 'Name', [
                                    'class' => 'form-required, control-label'
                                ])
                            }}
                            <p>{{ $student->name }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('dob', 'Date of brith', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <div class="input-append date form_datetime_date">
                                <p>{{ $student->dob? $student->dob : '-' }}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('dop', 'Place of birth', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->dop? $student->dop : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('language', 'Language', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->language? $student->language : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('race', 'Race', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->race? $student->race : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('gender', 'Gender', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->gender? $student->gender : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('type', 'Type', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->type? $student->type : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('registered', 'Registered date', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <div class="input-append date form_datetime">
                                <p>{{ $student->registered? $student->registered : '-' }}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('status', 'Status', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->status? $student->status : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('father_name', 'Father name', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->father_name? $student->father_name : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('father_contact', 'Father contanct no.', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>
                                <a href="tel: +{{ $student->father_contact }}">
                                    {{ $student->father_contact? $student->father_contact : '-' }}
                                </a>
                            </p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('mother_name', 'Mother name', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->mother_name? $student->mother_name : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('mother_contact', 'Mother contanct no.', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>
                                <a href="tel: +{{ $student->mother_contact }}">
                                    {{ $student->mother_contact? $student->mother_contact : '-' }}
                                </a>
                            </p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('address1', 'Address 1', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->address1? $student->address1 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            {{
                                Form::label('address2', 'Address 2', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->address2? $student->address2 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-12">
                            {{
                                Form::label('remarks1', 'Health status', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->remarks1? $student->remarks1 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-12">
                            {{
                                Form::label('remarks2', 'Academic record', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->remarks2? $student->remarks2 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-12">
                            {{
                                Form::label('remarks3', 'Special remarks', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->remarks3? $student->remarks3 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-12">
                            <a href="#" id="restoreConfirmation" class="btn btn-block btn-warning" aria-label="Left Align">
                                Restore
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            {!! Form::model($student, ['route' => ['restorestudents.restore', $student->id], 'method' => 'post', 'id' => 'restoreStudent']) !!}
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('page-component')
<script>
$(function () {
    $('#restoreConfirmation').click(function (e) {
        var msg = "Do you confirm you would like to restore selected student?";
        var confirmation = confirm(msg);
        if (confirmation) {
            var studentForm = $('#restoreStudent');
            studentForm.submit();
        } else {
            return false;
        }
    });
});
</script>
@endsection
