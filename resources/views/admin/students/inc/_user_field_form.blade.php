<div class="form-group col-md-12">
    <h3 class="box-title">Parent Login Info:</h3>
</div>
<div class="form-group col-md-6 {{ count($errors->get('username')) ? 'has-error': '' }}">
    {{
        Form::label('username', 'username', [
            'class' => 'form-required, control-label'
        ])
    }}
    {{
        Form::text('username', $user->username, [
            'class' => 'form-control',
            'placeholder' => 'Parent login username'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('profile_name')) ? 'has-error': '' }}">
    {{
        Form::label('profile_name', 'profile name', [
            'class' => 'form-required, control-label'
        ])
    }}
    {{
        Form::text('profile_name', $user->profile_name, [
            'class' => 'form-control',
            'placeholder' => 'Profle name for login account'
        ])
    }}
</div>
<div class="form-group col-md-12 {{ count($errors->get('email')) ? 'has-error': '' }}">
    {{
        Form::label('email', 'email', [
            'class' => 'form-required, control-label'
        ])
    }}
    {{
        Form::email('email', $user->email, [
            'class' => 'form-control',
            'placeholder' => 'Parent email for resetting password, received email, etc'
        ])
    }}
</div>
<div class="form-group col-md-12">
    <h3 class="box-title">Student Info:</h3>
</div>
