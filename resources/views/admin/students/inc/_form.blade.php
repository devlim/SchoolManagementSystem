<div class="form-group col-md-6 {{ count($errors->get('code')) ? 'has-error': '' }}">
    {{
        Form::label('code', 'Code', [
            'class' => 'form-required, control-label'
        ])
    }}
    {{
        Form::text('code', $student->code, [
            'class' => 'form-control',
            'placeholder' => 'Student code'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('name')) ? 'has-error': '' }}">
    {{
        Form::label('name', 'Name', [
            'class' => 'form-required, control-label'
        ])
    }}
    {{
        Form::text('name', $student->name, [
            'class' => 'form-control',
            'placeholder' => 'Student name'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('dob')) ? 'has-error': '' }}">
    {{
        Form::label('dob', 'Date of brith', [
            'class' => 'control-label'
        ])
    }}
    <div class="input-append date form_datetime_date">
        {{
            Form::text('dob', $student->dob, [
                'class' => 'form-control',
                'placeholder' => 'Student date of birth',
                'size' => 16,
            ])
        }}
        <span class="add-on"><i class="icon-th"></i></span>
    </div>
</div>
<div class="form-group col-md-6 {{ count($errors->get('dop')) ? 'has-error': '' }}">
    {{
        Form::label('dop', 'Place of birth', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('dop', $student->dop, [
            'class' => 'form-control',
            'placeholder' => 'Student place of birth'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('language')) ? 'has-error': '' }}">
    {{
        Form::label('language', 'Language', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('language', $student->language, [
            'class' => 'form-control',
            'placeholder' => 'Student language'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('race')) ? 'has-error': '' }}">
    {{
        Form::label('race', 'Race', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('race', $student->race, [
            'class' => 'form-control',
            'placeholder' => 'Student race'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('gender')) ? 'has-error': '' }}">
    {{
        Form::label('gender', 'Gender', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::select(
            'gender',
            [
                'Male' => 'Male',
                'Female' => 'Female',
            ],
            $student->gender,
            [
                'class' => 'form-control',
                'placeholder' => 'Please select',
            ]
        )
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('type')) ? 'has-error': '' }}">
    {{
        Form::label('type', 'Type', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('type', $student->type, [
            'class' => 'form-control',
            'placeholder' => 'Student type'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('registered')) ? 'has-error': '' }}">
    {{
        Form::label('registered', 'Registered date', [
            'class' => 'control-label'
        ])
    }}
    <div class="input-append date form_datetime">
        {{
            Form::text('registered', $student->registered, [
                'class' => 'form-control',
                'placeholder' => 'Student registered date'
            ])
        }}
        <span class="add-on"><i class="icon-th"></i></span>
    </div>
</div>
<div class="form-group col-md-6 {{ count($errors->get('status')) ? 'has-error': '' }}">
    {{
        Form::label('status', 'Status', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::select(
            'status',
            [
                'Active' => 'Active',
                'Non-Active' => 'Non-Active',
                'Achieved' => 'Achieved'
            ],
            $student->status,
            [
                'class' => 'form-control',
                'placeholder' => 'Please select',
            ]
        )
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('father_name')) ? 'has-error': '' }}">
    {{
        Form::label('father_name', 'Father name', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('father_name', $student->father_name, [
            'class' => 'form-control',
            'placeholder' => 'Student father name'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('father_contact')) ? 'has-error': '' }}">
    {{
        Form::label('father_contact', 'Father contanct no.', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('father_contact', $student->father_contact, [
            'class' => 'form-control',
            'placeholder' => 'Student father contanct no.'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('mother_name')) ? 'has-error': '' }}">
    {{
        Form::label('mother_name', 'Mother name', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('mother_name', $student->mother_name, [
            'class' => 'form-control',
            'placeholder' => 'Student mother name.'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('mother_contact')) ? 'has-error': '' }}">
    {{
        Form::label('mother_contact', 'Mother contanct no.', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::text('mother_contact', $student->mother_contact, [
            'class' => 'form-control',
            'placeholder' => 'Student mother contanct no.'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('address1')) ? 'has-error': '' }}">
    {{
        Form::label('address1', 'Address 1', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::textarea('address1', $student->address1, [
            'class' => 'form-control',
            'placeholder' => 'Student address 1'
        ])
    }}
</div>
<div class="form-group col-md-6 {{ count($errors->get('address2')) ? 'has-error': '' }}">
    {{
        Form::label('address2', 'Address 2', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::textarea('address2', $student->address2, [
            'class' => 'form-control',
            'placeholder' => 'Student address 2'
        ])
    }}
</div>
<div class="form-group col-md-12 {{ count($errors->get('remarks1')) ? 'has-error': '' }}">
    {{
        Form::label('remarks1', 'Health status', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::textarea('remarks1', $student->remarks1, [
            'class' => 'form-control',
            'placeholder' => 'Student health status'
        ])
    }}
</div>
<div class="form-group col-md-12 {{ count($errors->get('remarks2')) ? 'has-error': '' }}">
    {{
        Form::label('remarks2', 'Academic record', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::textarea('remarks2', $student->remarks2, [
            'class' => 'form-control',
            'placeholder' => 'Student academic record'
        ])
    }}
</div>
<div class="form-group col-md-12 {{ count($errors->get('remarks3')) ? 'has-error': '' }}">
    {{
        Form::label('remarks3', 'Special remarks', [
            'class' => 'control-label'
        ])
    }}
    {{
        Form::textarea('remarks3', $student->remarks3, [
            'class' => 'form-control',
            'placeholder' => 'Special remarks'
        ])
    }}
</div>
