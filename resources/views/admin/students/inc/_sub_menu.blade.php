<div class="col-md-3">
    <a href="{{ route('students.activities.create', $student->id) }}" class="btn btn-primary btn-block margin-bottom">Compose Activity</a>
    <div class="box box-solid">
        <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
            <li>
                <a href="{{ route('students.show', $student->id) }}">
                    <i class="fa fa-file-text-o"></i> View Student Info
                </a>
            </li>
            <li>
                <a href="{{ route('students.activities.index', $student->id) }}">
                    <i class="fa fa-life-ring"></i> View Activity
                </a>
            </li>
            <li>
                <a href="{{ route('students.attendances.index', $student->id) }}">
                    <i class="fa fa-location-arrow"></i> View Attendances
                </a>
            </li>
        </ul>
        </div>
    </div>
</div>
