@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
          Student
        </h1>
    </section>
    <section class="content">
        <div class="row">
            @include('admin.students.inc._sub_menu', [
                'studentId' => $student->id
            ])
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group col-md-6 {{ count($errors->get('code')) ? 'has-error': '' }}">
                            {{
                                Form::label('code', 'Code', [
                                    'class' => 'form-required, control-label'
                                ])
                            }}
                            <p>{{ $student->code }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('name')) ? 'has-error': '' }}">
                            {{
                                Form::label('name', 'Name', [
                                    'class' => 'form-required, control-label'
                                ])
                            }}
                            <p>{{ $student->name }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('dob')) ? 'has-error': '' }}">
                            {{
                                Form::label('dob', 'Date of brith', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <div class="input-append date form_datetime_date">
                                <p>{{ $student->dob? $student->dob : '-' }}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('dop')) ? 'has-error': '' }}">
                            {{
                                Form::label('dop', 'Place of birth', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->dop? $student->dop : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('language')) ? 'has-error': '' }}">
                            {{
                                Form::label('language', 'Language', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->language? $student->language : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('race')) ? 'has-error': '' }}">
                            {{
                                Form::label('race', 'Race', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->race? $student->race : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('gender')) ? 'has-error': '' }}">
                            {{
                                Form::label('gender', 'Gender', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->gender? $student->gender : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('type')) ? 'has-error': '' }}">
                            {{
                                Form::label('type', 'Type', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->type? $student->type : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('registered')) ? 'has-error': '' }}">
                            {{
                                Form::label('registered', 'Registered date', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <div class="input-append date form_datetime">
                                <p>{{ $student->registered? $student->registered : '-' }}</p>
                            </div>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('status')) ? 'has-error': '' }}">
                            {{
                                Form::label('status', 'Status', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->status? $student->status : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('father_name')) ? 'has-error': '' }}">
                            {{
                                Form::label('father_name', 'Father name', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->father_name? $student->father_name : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('father_contact')) ? 'has-error': '' }}">
                            {{
                                Form::label('father_contact', 'Father contanct no.', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>
                                <a href="tel: +{{ $student->father_contact }}">
                                    {{ $student->father_contact? $student->father_contact : '-' }}
                                </a>
                            </p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('mother_name')) ? 'has-error': '' }}">
                            {{
                                Form::label('mother_name', 'Mother name', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->mother_name? $student->mother_name : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('mother_name')) ? 'has-error': '' }}">
                            {{
                                Form::label('mother_contact', 'Mother contanct no.', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>
                                <a href="tel: +{{ $student->mother_contact }}">
                                    {{ $student->mother_contact? $student->mother_contact : '-' }}
                                </a>
                            </p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('address1')) ? 'has-error': '' }}">
                            {{
                                Form::label('address1', 'Address 1', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->address1? $student->address1 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6 {{ count($errors->get('address2')) ? 'has-error': '' }}">
                            {{
                                Form::label('address2', 'Address 2', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->address2? $student->address2 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-12 {{ count($errors->get('remarks1')) ? 'has-error': '' }}">
                            {{
                                Form::label('remarks1', 'Health status', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->remarks1? $student->remarks1 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-12 {{ count($errors->get('remarks2')) ? 'has-error': '' }}">
                            {{
                                Form::label('remarks2', 'Academic record', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->remarks2? $student->remarks2 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-12 {{ count($errors->get('remarks3')) ? 'has-error': '' }}">
                            {{
                                Form::label('remarks3', 'Special remarks', [
                                    'class' => 'control-label'
                                ])
                            }}
                            <p>{{ $student->remarks3? $student->remarks3 : '-' }}</p>
                        </div>
                        <div class="form-group col-md-6">
                            <a href="#" id="deleteConfirmation" class="btn btn-block btn-danger" aria-label="Left Align">
                                Delete
                            </a>
                        </div>
                        <div class="form-group col-md-6">
                            <a href="{{ route('students.edit', $student->id) }}" class="btn btn-block btn-primary" aria-label="Left Align">
                                Edit
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            {!! Form::model($student, ['route' => ['students.destroy', $student->id], 'method' => 'delete', 'id' => 'deleteStudent']) !!}
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('page-component')
<script>
$(function () {
    $('#deleteConfirmation').click(function (e) {
        var msg = "Deleting student will also delete parent login account and all student actitivies. \n" +
            "This action cannot be undone. \n" +
            "Are you sure you want to continue?"
        var confirmation = confirm(msg);
        if (confirmation) {
          var studentForm = $('#deleteStudent');
          studentForm.submit();
        } else {
        return false;
        }
    });
});
</script>
@endsection
