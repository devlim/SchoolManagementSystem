@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
            Edit Student
        </h1>
    </section>
    <section class="content">
        <div class="row">
            @include('admin.students.inc._sub_menu')
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-12">
                            @include('layouts.errors._form_errors_summary')
                        </div>
                        {!! Form::open(['route' => ['students.update', $student->id], 'method' => 'patch']) !!}
                            @include('admin.students.inc._form')
                            <div class="form-group col-md-6">
                                <a href="#" id="deleteConfirmation" class="btn btn-block btn-danger" aria-label="Left Align">
                                    Delete
                                </a>
                            </div>
                            <div class="form-group col-md-6">
                                {{
                                    Form::submit('Update', [
                                        'class' => 'btn btn-block btn-primary'
                                    ])
                                }}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div>
            {!! Form::model($student, ['route' => ['students.destroy', $student->id], 'method' => 'delete', 'id' => 'deleteStudent']) !!}
            {!! Form::close() !!}
        </div>
    </section>
@endsection

@section('page-component')
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.js') }}"></script>
<script>
$(function () {
    $('.form_datetime_date').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayBtn: true,
        minView: 2,
        viewSelect: 'year',
        startView: 4,
        autoclose: true,
    });

    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii:ss",
        autoclose: true,
        todayBtn: true,
        showMeridian: true
    });

    // Temp fix
    $('.icon-arrow-left').each(function () {
        $(this).attr('class', ' glyphicon glyphicon-arrow-left');
    });
    $('.icon-arrow-right').each(function () {
        $(this).attr('class', ' glyphicon glyphicon-arrow-right');
    });

    $('#deleteConfirmation').click(function (e) {
        var msg = "Deleting student will also delete parent login account and all student actitivies. \n" +
            "This action cannot be undone. \n" +
            "Are you sure you want to continue?"
        var confirmation = confirm(msg);
        if (confirmation) {
          var studentForm = $('#deleteStudent');
          studentForm.submit();
        } else {
        return false;
        }
    });
});
</script>
@endsection
