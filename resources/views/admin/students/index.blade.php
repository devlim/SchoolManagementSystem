@extends('layouts.app')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection
@section('content')
<section class="content-header">
    @include('flash::message')
    <h1>
      Students Listing
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="student_listing">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Race</th>
                                <th>Gender</th>
                                <th>Registered</th>
                                <th>Status</th>
                                <th>View</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if (count($model) != 0)
                                    @foreach ($model as $i => $student)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $student->code }}</a></td>
                                            <td>{{ $student->name }}</td>
                                            <td>{{ $student->race }}</td>
                                            <td>{{ $student->gender }}</td>
                                            <td>{{ $student->registered }}</td>
                                            <td>{{ $student->status }}</td>
                                            <td><a href="{{ route('students.show', $student->id) }}">View<a></td>
                                            <td><a href="{{ route('students.edit', $student->id) }}">Edit<a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
  jQuery(function () {
    jQuery('.dataTable').DataTable()
  })();
</script>
@endsection
