@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
          Add New Student
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-12">
                                @include('layouts.errors._form_errors_summary')
                            </div>
                            {!! Form::open(['route' => 'students.store', 'method' => 'post']) !!}
                                @include('admin.students.inc._user_field_form')
                                @include('admin.students.inc._form')
                                <div class="form-group col-md-offset-6">
                                    {{
                                        Form::submit('Create', [
                                            'class' => 'btn btn-block btn-primary'
                                        ])
                                    }}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page-component')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/css/bootstrap-datetimepicker.css" media="screen" title="no title">
<script src="https://cdnjs.cloudflare.com/ajax/libs/smalot-bootstrap-datetimepicker/2.4.4/js/bootstrap-datetimepicker.js"></script>
<script>
$(function () {
    $('.form_datetime_date').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayBtn: true,
        minView: 2,
        viewSelect: 'year',
        startView: 4,
        autoclose: true,
    });

    $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii:ss",
        autoclose: true,
        todayBtn: true,
        showMeridian: true
    });

    // Temp fix
    $('.icon-arrow-left').each(function () {
        $(this).attr('class', ' glyphicon glyphicon-arrow-left');
    });
    $('.icon-arrow-right').each(function () {
        $(this).attr('class', ' glyphicon glyphicon-arrow-right');
    });
});
</script>
@endsection
