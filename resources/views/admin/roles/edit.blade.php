@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
          Edit Role
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-12">
                                @include('layouts.errors._form_errors_summary')
                            </div>
                            {!! Form::model($role, ['route' => ['roles.update', $role->id], 'method' => 'patch']) !!}
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-hover" role="grid" aria-describedby="role_listing">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Access Right</th>
                                                <th>Enabled</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($role->permissions) != 0)
                                                    @foreach ($role->permissions as $i => $permission)
                                                        <tr style="{{
                                                            count($errors->get('Permission.' . $i . '.student_id'))?
                                                                'border-left: #dd4b39 5px solid;': '' }}">
                                                            <td>{{ $i + 1 }}</td>
                                                            <td>{{ $permission_labels[$permission->ability] }}</td>
                                                            <td>
                                                                {{
                                                                    Form::checkbox(
                                                                        'permission_enabled[' . $i . ']',
                                                                        $permission->enabled,
                                                                        $permission->enabled,
                                                                        [
                                                                            'data-num' => $i,
                                                                            'class' => 'permission_enabled'
                                                                        ]
                                                                    )
                                                                }}
                                                                {{
                                                                    Form::hidden(
                                                                        'Permission[' . $i . '][enabled]',
                                                                        $permission->enabled? $permission->enabled : 0,
                                                                        [
                                                                            'id' => 'permissions_' . $i
                                                                        ]
                                                                    )
                                                                }}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    {{
                                        Form::submit('Update', [
                                            'class' => 'btn btn-block btn-primary'
                                        ])
                                    }}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('page-component')
<script>
$(function () {
    jQuery(function () {
        jQuery('.permission_enabled').click(function (e) {
            var checked = $(this).prop('checked');
            var num = $(this).attr('data-num');

            var el = document.getElementById('permissions_' + num);
            if (checked) {
                el.value = 1;
            } else {
                el.value = 0;
            }
        });
    })();
});
</script>
@endsection
