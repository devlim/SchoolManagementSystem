@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
          View Role
        </h1>
    </section>
    <section class="content">
        <div class="box">
            <div class="row">
                <div class="form-group col-md-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="col-md-12">
                                @include('layouts.errors._form_errors_summary')
                            </div>
                            <div class="form-group col-md-12">
                                {{
                                    Form::label('Role[Name]', 'Role name', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                <p>
                                    {{ $role->name }}
                                </p>
                            </div>
                            <div class="form-group col-md-6">
                                {{
                                    Form::label('Role[created_at]', 'Created At', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                <p>
                                    {{ $role->created_at }}
                                </p>
                            </div>
                            <div class="form-group col-md-6">
                                {{
                                    Form::label('Role[updated_at]', 'Updated At', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                <p>
                                    {{ $role->updated_at }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover" role="grid" aria-describedby="student_listing">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Access Right</th>
                                                    <th>Enabled</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @if ($role->permissions->isEmpty() == false)
                                                        @foreach ($role->permissions as $i => $permission)
                                                            <tr>
                                                                <td>{{ $i + 1 }}</td>
                                                                <td>{{ $permission_labels[$permission->ability] }}</td>
                                                                <td>
                                                                    @if ($permission->enabled == true)
                                                                        <i class="fa fa-check" aria-hidden="true" style="color: #4CAF4F"></i>
                                                                    @else
                                                                        <i class="fa fa-times" aria-hidden="true" style="color: #f44336"></i>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-block btn-primary" aria-label="Left Align">
                                    Edit
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
