@extends('layouts.app')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
    @include('flash::message')
    <h1>
      Role Listing
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="role_listing">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>View</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if ($roles->isEmpty() == false)
                                    @foreach ($roles as $i => $role)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $role->name }}</td>
                                            <td>{{ $role->created_at }}</td>
                                            <td>{{ $role->updated_at }}</td>
                                            <td><a href="{{ route('roles.show', $role->id) }}">View<a></td>
                                            <td><a href="{{ route('roles.edit', $role->id) }}">Edit<a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
  jQuery(function () {
    jQuery('.dataTable').DataTable()
  })();
</script>
@endsection
