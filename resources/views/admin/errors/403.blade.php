@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
          403 Errors!
        </h1>
    </section>
@endsection
