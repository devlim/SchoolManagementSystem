@extends('layouts.app_auth')

@section('content')
<p class="login-box-msg">
	Dear operator, 
	<br />
	please fill out following form for reset your password
</p>
{!! Form::open(['route' => 'admin.password.request', 'method' => 'post']) !!}
  <input type="hidden" name="token" value="{{ $token }}">
  <div class="form-group has-feedback {{ $errors->has('email')? 'has-error' : ''}}">
    {{
      Form::email('email', old('email'), [
        'class' => 'form-control',
        'placeholder' => 'Email',
        'required',
        'autofocus',
      ])
    }}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    @if ($errors->has('email'))
      <span class="help-block">
        {{ $errors->first('email') }}
      </span>
    @endif
  </div>
  <div class="form-group has-feedback {{ $errors->has('password')? 'has-error' : ''}}">
    {{
      Form::password('password', [
        'class' => 'form-control',
        'placeholder' => 'Password',
        'required',
      ])
    }}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    @if ($errors->has('password'))
      <span class="help-block">
        {{ $errors->first('password') }}
      </span>
    @endif
  </div>
  <div class="form-group has-feedback {{ $errors->has('password_confirmation')? 'has-error' : ''}}">
    {{
      Form::password('password_confirmation', [
        'class' => 'form-control',
        'placeholder' => 'Password Confirmation',
        'required',
      ])
    }}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    @if ($errors->has('password_confirmation'))
      <span class="help-block">
        {{ $errors->first('password_confirmation') }}
      </span>
    @endif
  </div>
  <div class="row">
    <div class="col-xs-offset-4 col-xs-8">
      {{
          Form::submit('Reset Password', [
              'class' => 'btn btn-primary btn-block btn-flat'
          ])
      }}
    </div>
  </div>
{!! Form::close() !!}
@endsection