@extends('layouts.app_auth')

@section('content')
<p class="login-box-msg">
	Dear operator, 
	<br />
	please enter your email for reset password
</p>
{!! Form::open(['route' => 'admin.password.email', 'method' => 'post']) !!}
	@if (session('status'))
		<div class="callout callout-success">
			<p>{{ session('status') }}</p>
        </div>
	@endif
  <div class="form-group has-feedback {{ $errors->has('email')? 'has-error' : ''}}">
    {{
      Form::email('email', old('email'), [
        'class' => 'form-control',
        'placeholder' => 'Email',
        'required',
        'autofocus',
      ])
    }}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    @if ($errors->has('email'))
      <span class="help-block">
        {{ $errors->first('email') }}
      </span>
    @endif
  </div>
  <div class="row">
    <div class="col-xs-offset-4 col-xs-8">
      {{
          Form::submit('Send Password Reset Link', [
              'class' => 'btn btn-primary btn-block btn-flat'
          ])
      }}
    </div>
  </div>
{!! Form::close() !!}
@endsection