@extends('layouts.app_auth')

@section('content')
<p class="login-box-msg">Dear parent, please sign in to start your session</p>
{!! Form::open(['route' => 'login', 'method' => 'post']) !!}
  <div class="form-group has-feedback {{ $errors->has('username')? 'has-error' : ''}}">
    {{
      Form::text('username', old('username'), [
        'class' => 'form-control',
        'placeholder' => 'Username',
        'required',
        'autofocus',
      ])
    }}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    @if ($errors->has('username'))
      <span class="help-block">
        {{ $errors->first('username') }}
      </span>
    @endif
  </div>
  <div class="form-group has-feedback {{ $errors->has('password')? 'has-error' : ''}}">
    {{
      Form::password('password', [
        'class' => 'form-control',
        'placeholder' => 'Password',
        'required',
      ])
    }}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    @if ($errors->has('password'))
      <span class="help-block">
        {{ $errors->first('password') }}
      </span>
    @endif
  </div>
  <div class="row">
    <div class="col-xs-8">
      <div class="checkbox icheck">
        <label>
          {{
            Form::checkbox('remember', (old('remember') ? 'checked' : ''))
          }} Remember Me
        </label>
      </div>
    </div>

    <div class="col-xs-4">
      {{
          Form::submit('Sign In', [
              'class' => 'btn btn-primary btn-block btn-flat'
          ])
      }}
    </div>
  </div>
{!! Form::close() !!}
<a href="{{ route('password.request') }}">I forgot my password</a><br>
@endsection
