@extends('layouts.app_auth')

@section('content')
<p class="login-box-msg">Dear operator, <br />please sign in to start your session</p>
{!! Form::open(['route' => 'admin.login.submit', 'method' => 'post']) !!}
  <div class="form-group has-feedback {{ $errors->has('email')? 'has-error' : ''}}">
    {{
      Form::email('email', old('email'), [
        'class' => 'form-control',
        'placeholder' => 'Email',
        'required',
        'autofocus',
      ])
    }}
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    @if ($errors->has('email'))
      <span class="help-block">
        {{ $errors->first('email') }}
      </span>
    @endif
  </div>
  <div class="form-group has-feedback {{ $errors->has('password')? 'has-error' : ''}}">
    {{
      Form::password('password', [
        'class' => 'form-control',
        'placeholder' => 'Password',
        'required',
      ])
    }}
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    @if ($errors->has('password'))
      <span class="help-block">
        {{ $errors->first('password') }}
      </span>
    @endif
  </div>
  <div class="row">
    <div class="col-xs-8">
      <div class="checkbox icheck">
        <label>
          {{
            Form::checkbox('remember', (old('remember') ? 'checked' : ''))
          }} Remember Me
        </label>
      </div>
    </div>

    <div class="col-xs-4">
      {{
          Form::submit('Sign In', [
              'class' => 'btn btn-primary btn-block btn-flat'
          ])
      }}
    </div>
  </div>
{!! Form::close() !!}
<a href="{{ route('admin.password.request') }}">I forgot my password</a><br>
@endsection