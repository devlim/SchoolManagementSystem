@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Children Activities: {{ $student->name }}
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @include(
                    'layouts.form._filter_date',
                    [
                        'header_title' => 'Filter activities by date',
                        'route_params' => 'parent.children.activities',
                        'filter_date_param' => 'filter_date',
                        'filter_date_param_placholder' => 'Filter actitivies by date',
                    ]
                )
                @if (count($activities) > 0)
                    <ul class="timeline">
                        <li class="time-label">
                            <span class="bg-red">
                                {{ $filter_date }}
                            </span>
                        </li>
                        @foreach ($activities as $i => $activity)
                            <li>
                            <i class="fa fa-life-ring bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time">
                                        <i class="fa fa-clock-o"></i> {{ $activity->timeline->format('h:i:s A') }}
                                    </span>
                                    <br/>
                                    <div class="timeline-body">
                                        {!! nl2br(e($activity->log)) !!}
                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                </div>
                            </li>
                        @endforeach
                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>
                        </li>
                    </ul>
                @else
                <div class="box">
                    <div class="box-body">
                        There isn't any activies {{ $filter_date }}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </section>
@endsection

@section('page-component')
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>

<script>
    $('#datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        todayBtn: 'linked'
    });
</script>
@endsection
