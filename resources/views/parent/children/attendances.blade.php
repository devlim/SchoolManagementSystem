@extends('layouts.app')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
    <h1>
      Attendance By Month
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            @include(
                'layouts.form._filter_date',
                [
                    'header_title' => 'Filter activities by month',
                    'route_params' => 'parent.children.attendances',
                    'filter_date_param' => 'filter_date',
                    'filter_date_param_placholder' => 'Filter attendances by month',
                ]
            )
            <div class="box">
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="attendance_listing_by_month">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Attendance</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if ($attendances->isEmpty() == false)
                                    @foreach ($attendances as $i => $attendance)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $attendance->date }}</a></td>
                                            <td data-order="{{ $attendance->attend == true? 1 : 0 }}">
                                                @if ($attendance->attend == true)
                                                    <i class="fa fa-check" aria-hidden="true" style="color: #4CAF4F"></i>
                                                @else
                                                    <i class="fa fa-times" aria-hidden="true" style="color: #f44336"></i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


@section('page-component')
<!-- Datepicker -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script>
  jQuery(function () {
    jQuery('.dataTable').DataTable();

    jQuery("#datepicker").datepicker({
        autoclose: true,
        format: "mm-yyyy",
        viewMode: "months",
        minViewMode: "months"
    });
  })();
</script>
@endsection
