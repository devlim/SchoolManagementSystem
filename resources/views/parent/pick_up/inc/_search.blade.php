<!-- Implement later -->
<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Filter Submitted Pick Up Forms by Year</h3>
    </div>
    <div class="box-body">
        @include('layouts.errors._form_errors_summary')

        {!! Form::open(['route' => ['parent.pick_ups.history'], 'method' => 'get']) !!}
        <div class="form-group">
            <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                {{
                    Form::text('filter_year', null, [
                        'id' => 'datepicker',
                        'class' => 'form-control pull-right',
                        'placeholder' => 'Filter Submitted Pick Up Forms By Year'
                    ])
                }}
            </div>
        </div>
        <div class="form-group">
            {{
                Form::submit('Filter', [
                    'class' => 'btn btn-primary pull-right'
                ])
            }}
        </div>
        {!! Form::close() !!}
    </div>
</div>
