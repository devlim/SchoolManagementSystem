@extends('layouts.app')

@section('content')
    <section class="content-header">
        @include('flash::message')
        <h1>
          {{ $today_date }} Submitted Pick Up Form
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <table class="table" role="grid" aria-describedby="today_submitted_pick_up_form_listing">
                            <thead>
                            <tr>
                                <th>Driver Name</th>
                                <th>Vehicle Plate Num</th>
                                <th>Relationship</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if (count($today_pick_ups) != 0)
                                    @foreach ($today_pick_ups as $today_pick_up)
                                    <tr>
                                        <td>{{ $today_pick_up->driver_name}}</td>
                                        <td>{{ $today_pick_up->vehicle_plate_num }}</a></td>
                                        <td>{{ $today_pick_up->relationship }}</td>
                                        <td>{{ $today_pick_up->status }}</td>
                                        <td>
                                            <a href="{{ route('parent.pick_ups.show', $today_pick_up->id) }}">View<a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td spancol="5">No record found.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content-header">
        <h1>
          Pick Up Form
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @if (count($active_today_pick_up) == 0)
                        <div class="col-md-12">
                            @include('layouts.errors._form_errors_summary')
                        </div>
                        {!!
                            Form::model(
                                $pick_up,
                                [
                                    'route' => [
                                        'parent.pick_ups.store',
                                        $pick_up,
                                    ]
                                ]
                            )
                        !!}
                            <div class="form-group col-md-6">
                                {{
                                    Form::label('submit_date', 'Submit Date \\ Pick Up Date', [
                                        'class' => 'form-required, control-label'
                                    ])
                                }}
                                <p>{{ $pick_up->submit_date }}</p>
                            </div>
                            <div class="form-group col-md-6 {{ count($errors->get('driver_name')) ? 'has-error': '' }}">
                                {{
                                    Form::label('driver_name', 'Driver Name', [
                                        'class' => 'form-required, control-label'
                                    ])
                                }}
                                {{
                                    Form::text('driver_name', $pick_up->driver_name, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Driver Name'
                                    ])
                                }}
                            </div>
                            <div class="form-group col-md-6 {{ count($errors->get('vehicle_plate_num')) ? 'has-error': '' }}">
                                {{
                                    Form::label('vehicle_plate_num', 'Vehicle Plate Num', [
                                        'class' => 'form-required, control-label'
                                    ])
                                }}
                                {{
                                    Form::text('vehicle_plate_num', $pick_up->vehicle_plate_num, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Vehicle Plate Name'
                                    ])
                                }}
                            </div>
                            <div class="form-group col-md-6 {{ count($errors->get('relationship')) ? 'has-error': '' }}">
                                {{
                                    Form::label('relationship', 'Relationship', [
                                        'class' => 'form-required, control-label'
                                    ])
                                }}
                                {{
                                    Form::text('relationship', $pick_up->relationship, [
                                        'class' => 'form-control',
                                        'placeholder' => 'relationship'
                                    ])
                                }}
                            </div>
                            <div class="form-group col-md-12 {{ count($errors->get('comment')) ? 'has-error': '' }}">
                                {{
                                    Form::label('comment', 'comment', [
                                        'class' => 'control-label'
                                    ])
                                }}
                                {{
                                    Form::textarea('comment', $pick_up->comment, [
                                        'class' => 'form-control',
                                        'placeholder' => 'Comment'
                                    ])
                                }}
                            </div>
                            <div class="form-group col-md-offset-6">
                                {{
                                    Form::submit('Submit', [
                                        'class' => 'btn btn-block btn-primary'
                                    ])
                                }}
                                <p class="text-center">
                                    <i>&#42; please make sure that all fields are filled out correctly and submitted before entertain hours set by school.</i>
                                </p>
                            </div>
                        {!! Form::close() !!}
                        @else
                            <div class="col-md-12">
                                <p class="text-center">
                                    In order to avoid misunderstanding, only one active pick up form is allowed. <br />
                                    Please cancel any existing active pick-up form first before creating a new one. <br />
                                    Thank you.
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
