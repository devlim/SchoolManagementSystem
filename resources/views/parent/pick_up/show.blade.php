@extends('layouts.app')

@section('content')
<section class="content-header">
    @include('flash::message')
    <h1>
      Pick Up
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-group col-md-12">
                        {{
                            Form::label('submit_date', 'Submit Date', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->submit_date }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('driver_name', 'Driver Name', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->driver_name }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('vehicle_plate_num', 'Vehicle Plate Num', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->vehicle_plate_num }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('relationship', 'Relationship', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->relationship }}</p>
                    </div>
                    <div class="form-group col-md-6">
                        {{
                            Form::label('status', 'Status', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->status }}</p>
                    </div>
                    @if ($pick_up->status != 'cancel')
                    <div class="form-group col-md-12">
                    @else
                    <div class="form-group col-md-6">
                    @endif
                        {{
                            Form::label('created_at', 'Created at', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->created_at }}</p>
                    </div>
                    @if ($pick_up->status == 'cancel')
                    <div class="form-group col-md-6">
                        {{
                            Form::label('cancel_at', 'Cancel at', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->cancel_at }}</p>
                    </div>
                    @endif
                    <div class="form-group col-md-12">
                        {{
                            Form::label('comment', 'Comment', [
                                'class' => 'control-label'
                            ])
                        }}
                        <p>{{ $pick_up->comment? $pick_up->comment : '-' }}</p>
                    </div>
                    @if ($pick_up->status != 'cancel')
                        <div class="form-group col-md-12">
                            {!! Form::model($pick_up, ['route' => ['parent.pick_ups.cancel', $pick_up->id], 'method' => 'post']) !!}
                                {{
                                    Form::submit('Cancel', [
                                        'class' => 'btn btn-block btn-danger',
                                        'id' => 'cancelConfirmation'
                                    ])
                                }}
                            {!! Form::close() !!}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-component')
<script>
$(function () {
    $('#cancelConfirmation').click(function (e) {
        var confirmation = confirm("Are you sure you want to cancel?");
        if (confirmation) {
            return true;
        } else {
            return false;
        }
    });
});
</script>
@endsection
