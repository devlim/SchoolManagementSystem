@extends('layouts.app')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<section class="content-header">
    <h1>
      Pick Up Form History Listing
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap table-responsive">
                        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="pick_up_form_history_listing">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Submit Date</th>
                                <th>Cancel At</th>
                                <th>Status</th>
                                <th>View</th>
                            </tr>
                            </thead>
                            <tbody>
                                @if (count($pick_ups) != 0)
                                    @foreach ($pick_ups as $i => $pick_up)
                                        <tr>
                                            <td>{{ $i + 1 }}</td>
                                            <td>{{ $pick_up->submit_date }}</a></td>
                                            <td>{{ $pick_up->cancel_at? $pick_up->cancel_at : '-' }}</td>
                                            <td>{{ $pick_up->status }}</td>
                                            <td><a href="{{ route('parent.pick_ups.show', $pick_up->id) }}">View<a></td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<!-- DataTables -->
<script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Bootstrap Datepicker -->
<link rel="stylesheet" href="{{ asset('plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" media="screen" title="no title" />
<script src="{{ asset('plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>

<script>
  jQuery(function () {
    jQuery('.dataTable').DataTable();

    jQuery('#datepicker').datepicker({
        autoclose: true,
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
  })();
</script>
@endsection
