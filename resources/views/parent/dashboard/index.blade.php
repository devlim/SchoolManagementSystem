@extends('layouts.app')

@section('content')
<section class="content-header">
    @include('flash::message')
    <h1>
      Parent Dashboard
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Welcome</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            Welcome to parent dashboard!
            <br />
            You are logged in as Parent!
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (count($today_activities) > 0)
                <ul class="timeline">
                    <li class="time-label">
                        <span class="bg-red">
                            {{ $today_date }}
                        </span>
                    </li>
                    @foreach ($today_activities as $i => $activity)
                        <li>
                        <i class="fa fa-life-ring bg-blue"></i>
                            <div class="timeline-item">
                                <span class="time">
                                    <i class="fa fa-clock-o"></i> {{ $activity->timeline->format('h:i:s A') }}
                                </span>
                                <br/>
                                <div class="timeline-body">
                                    {!! nl2br(e($activity->log)) !!}
                                </div>
                            </div>
                        </li>
                    @endforeach
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>
            @else
            <div class="box">
                <div class="box-body">
                    There isn't any activies {{ $today_date }}
                </div>
            </div>
            @endif
        </div>
    </div>
</section>
@endsection
