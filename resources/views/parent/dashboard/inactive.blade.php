@extends('layouts.app')

@section('content')
<section class="content-header">
    <h1>
      Parent Dashboard
    </h1>
</section>
<section class="content">
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Notice</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <p>
                Your account associated with student code:
                <b>{{ $student->code }}</b>
                , student name:
                <b>{{ $student->name }}</b>
                has been deactivated on <b>{{ $student->deleted_at }}.</b>
                <br />
            </p>
            <p>
                If you believe there is a mistake or misunderstanding, please contact school for more information.
            </p>
        </div>
    </div>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Welcome</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            Welcome to parent dashboard!
            <br />
            You are logged in as Parent!
        </div>
    </div>
</section>
@endsection
