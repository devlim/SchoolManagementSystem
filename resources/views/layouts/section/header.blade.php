<?php
if (Auth::guard('web')->check() == 1) {
    // parent route
    $logo_route = route('parent.dashboard');
    $logout_route = route('logout');
    $login_name = Auth::user()->profile_name;
} elseif (Auth::guard('admin')->check() == 1) {
    // admin route
    $logo_route = route('admin.dashboard');
    $logout_route = route('admin.logout');
    $login_name = Auth::user()->name;
}
?>

<header class="main-header">

        <!-- Logo -->
    <a href="{{ $logo_route }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <b>SMS</b>
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <b>SMS</b>
        </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span>
                            {{ $login_name }}
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-footer">
                            <div class="pull-right">
                                <a href="{{ $logout_route }}"
                                    class="btn btn-default btn-flat"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Sign out
                                </a>

                            <form id="logout-form"
                                action="{{ $logout_route }}"
                                method="POST"
                                style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
