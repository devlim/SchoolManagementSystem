<aside class="main-sidebar">
<!-- sidebar -->
<section class="sidebar">
<!-- sidebar menu -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        @if (Auth::guard('admin')->check() == 1 and
            Auth::guard('web')->check() == 0)
        <li class="active">
            <a href="{{ route('admin.dashboard' )}}">
                <i class="fa fa-th"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-graduation-cap"></i> <span>Students</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('students.index') }}">
                        <i class="fa fa-list-ol"></i>
                        Students List
                    </a>
                </li>
                <li>
                    <a href="{{ route('students.create') }}">
                        <i class="fa fa-plus"></i>
                        Add New Student
                    </a>
                </li>
                <li>
                    <a href="{{ route('restorestudents.index') }}">
                        <i class="fa fa-stack-overflow"></i>
                        Restore Students
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-location-arrow"></i> <span>Attendances</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('attendances.index') }}">
                        <i class="fa fa-list-ol"></i>
                        Attendances List
                    </a>
                </li>
                <li>
                    <a href="{{ route('attendances.create') }}">
                        <i class="fa fa-plus"></i>
                        Generate Attendance
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-car"></i> <span>Pick Ups</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('pick_ups.index') }}">
                        <i class="fa fa-list-ol"></i>
                        Pick Ups List
                    </a>
                </li>
            </ul>
        </li>
        <li class="header">Admin Setting</li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-user-circle-o"></i> <span>Roles</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('roles.index') }}">
                        <i class="fa fa-list-ol"></i>
                        Roles List
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if (Auth::guard('web')->check() == 1 and
        Auth::guard('admin')->check() == 0)
        <li class="active">
            <a href="{{ route('parent.dashboard' )}}">
                <i class="fa fa-th"></i> <span>Dashboard</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-graduation-cap"></i> <span>Children</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('parent.children.profile') }}">
                        <i class="fa fa-child"></i>
                        profile
                    </a>
                </li>
                <li>
                    <a href="{{ route('parent.children.activities') }}">
                        <i class="fa fa-life-ring"></i>
                        Activities
                    </a>
                </li>
                <li>
                    <a href="{{ route('parent.children.attendances') }}">
                        <i class="fa fa-location-arrow"></i>
                        Attendances
                    </a>
                </li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-car"></i> <span>Pick Up</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li>
                    <a href="{{ route('parent.pick_ups.create') }}">
                        <i class="fa fa-file-text-o"></i>
                        Pick Up Form
                    </a>
                </li>
                <li>
                    <a href="{{ route('parent.pick_ups.history') }}">
                        <i class="fa fa-list-alt"></i>
                        Pick Up Form History
                    </a>
                </li>
            </ul>
        </li>
    @endif
    </ul>
</section>
<!-- /.sidebar -->
</aside>
