@if (count($errors) > 0)
    <div class="callout callout-danger" role="alert">
        @foreach ($errors->all() as $error)
            <div class="">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                <span class="sr-only">Error:</span>
                {{ $error }}
            </div>
        @endforeach
    </div>
@endif
