@component('mail::message')
# Hello!

Welcome to *School Management System*

You are receiving this email because we received your email during student registration process.

The following is your login detail as parent:

Username: {{ $user->username }}

Password: {{ $login_password }}

Your can login by clicking following button
@component('mail::button', ['url' => config('app.url') . '/login'])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
